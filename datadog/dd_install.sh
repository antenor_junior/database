#/bin/bash
#bash /lendico/datadog/dd_install.sh "/lendico/datadog" "SLAVE"
set -ex

DD_DIR=$1
PG_CLUSTER="MASTER"
if [ "$2" ]; then
  PG_CLUSTER=$2
fi

sudo yum install -y python-pip python3-pip python-psycopg2 fabric pg_activity sysstat
sudo pip install datadog

if [ $(rpm -qa | grep datadog | wc -l) == 0 ];
then
  echo "DD AGENT started install"
  DD_AGENT_MAJOR_VERSION=7 DD_API_KEY=74d38118dff10595f18785a158a26eb3 DD_SITE="datadoghq.com" bash -c "$(curl -L https://s3.amazonaws.com/dd-agent/scripts/install_script.sh)"
else
  echo "DD AGENT installed - nothing to do"
fi

sudo cp -u $DD_DIR/postgresql.yaml /etc/datadog-agent/conf.d/postgres.d/
sudo cp -u $DD_DIR/pgbouncer.yaml /etc/datadog-agent/conf.d/pgbouncer.d/
sudo cp -u $DD_DIR/system_core.yaml /etc/datadog-agent/conf.d/system_core.d/
sudo cp -u $DD_DIR/tcp_check.yaml /etc/datadog-agent/conf.d/tcp_check.d/
sudo cp -u $DD_DIR/system_swap.yaml /etc/datadog-agent/conf.d/system_swap.d/
sudo cp -u $DD_DIR/disk.yaml /etc/datadog-agent/conf.d/disk.d/
sudo cp -u $DD_DIR/dogstatsd_metrics.py /etc/datadog-agent/conf.d/
sudo cp -u $DD_DIR/pg_stats_bloat.py /etc/datadog-agent/conf.d/

echo "* * * * * root python2.7 /etc/datadog-agent/conf.d/dogstatsd_metrics.py" | sudo tee /etc/cron.d/dd_dogstatd_metrics
echo "0 * * * * root python2.7 /etc/datadog-agent/conf.d/pg_stats_bloat.py" | sudo tee /etc/cron.d/dd_pg_stats_bloat

if [ $PG_CLUSTER == "MASTER" ]; then
  sudo -iu postgres psql -c "CREATE ROLE datadog LOGIN PASSWORD 'datadog';"
  sudo -iu postgres psql -c "GRANT pg_monitor, pg_read_all_stats, pg_stat_scan_tables TO datadog;"
  sudo -iu postgres psql -c "GRANT SELECT ON pg_stat_database TO datadog;"
fi

sudo -iu postgres  echo "*:*:*:datadog:datadog" | sudo -iu postgres tee -a .pgpass

sudo -iu postgres psql -h localhost -U datadog postgres -c \
"select * from pg_stat_database LIMIT(1);" \
&& echo -e "\e[0;32mPostgres connection - OK\e[0m" \
|| echo -e "\e[0;31mCannot connect to Postgres\e[0m"

sudo systemctl restart datadog-agent