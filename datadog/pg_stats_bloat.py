import fabric
import time
import sys
import re
import os
import shutil
import datetime
from datadog import initialize, statsd
from subprocess import Popen, PIPE, STDOUT, check_output
from psycopg2 import connect, sql

options = {
    'statsd_host':'127.0.0.1',
    'statsd_port':8125
}

initialize(**options)

def connect_host(host):
  conn = fabric.Connection(host)
  conn.run('echo "Connected on $( hostname --fqdn ) - uptime: $( uptime )"')
  #conn.cd('/tmp')
  #conn.command_cwds = ['/tmp']
  return conn

def table_bloat_stats(host_name, rds_identifier):
  db_list_cmd = 'psql -Xqt -h ' + host_name + ' -d postgres -U datadog -c "SELECT datname FROM pg_database WHERE ( SELECT pg_is_in_recovery() ) IS FALSE AND datname !~* \'^(postgres|template|zabbix|rdsadmin)\' "'
  #print db_list_cmd
  db_list = Popen(db_list_cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for db in db_list.stdout:
    #print('tbl')
    #print(db.strip() + ' - ' + str(len(db.strip())) )
    if (len(db.strip()) == 0):
      continue
    pg_conn = connect(dbname = db.strip(), user = 'lendico_lgpd_full_ro', host = host_name )
    pg_curs = pg_conn.cursor()
    sqlfile = open ('/lendico/database/scripts/pg_table_bloat_datadog.sql', 'r')
    pg_curs.execute(sqlfile.read())
    table_data = pg_curs.fetchall()


    for num, row in enumerate(table_data):
      statsd.gauge('pgsql.bloat.table.real_size' , row[3] , tags=["pgsql_bloat_table_database:" + row[0], "pgsql_bloat_table_schema:" + row[1], "pgsql_bloat_table_tablename:" + row[2], "pgsql_bloat_table:pgsql.bloat.table", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.table.fillfactor' , row[4] , tags=["pgsql_bloat_table_database:" + row[0], "pgsql_bloat_table_schema:" + row[1], "pgsql_bloat_table_tablename:" + row[2], "pgsql_bloat_table:pgsql.bloat.table", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.table.bloat_size' , row[5] , tags=["pgsql_bloat_table_database:" + row[0], "pgsql_bloat_table_schema:" + row[1], "pgsql_bloat_table_tablename:" + row[2], "pgsql_bloat_table:pgsql.bloat.table", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.table.bloat_ratio' , row[6] , tags=["pgsql_bloat_table_database:" + row[0], "pgsql_bloat_table_schema:" + row[1], "pgsql_bloat_table_tablename:" + row[2], "pgsql_bloat_table:pgsql.bloat.table", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.table.hot_upd_rate' , row[7] , tags=["pgsql_bloat_table_database:" + row[0], "pgsql_bloat_table_schema:" + row[1], "pgsql_bloat_table_tablename:" + row[2], "pgsql_bloat_table:pgsql.bloat.table", "dbinstanceidentifier:" + rds_identifier ] )
      #print ("row: ", row)
      #print (type(row))

    pg_curs.close()
    pg_conn.close()
        #print insert_table_cmd

def index_bloat_stats(host_name, rds_identifier):
  db_list_cmd = 'psql -Xqt -h ' + host_name + ' -d postgres -U datadog -c "SELECT datname FROM pg_database WHERE ( SELECT pg_is_in_recovery() ) IS FALSE AND datname !~* \'^(postgres|template|zabbix|rdsadmin)\' "'
  #print db_list_cmd
  db_list = Popen(db_list_cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for db in db_list.stdout:
    #print('idx')
    #print(db.strip() + ' - ' + str(len(db.strip())) )
    if (len(db.strip()) == 0):
      continue
    pg_conn = connect(dbname = db.strip(), user = 'lendico_lgpd_full_ro', host = host_name )
    pg_curs = pg_conn.cursor()
    sqlfile = open ('/lendico/database/scripts/pg_index_bloat_datadog.sql', 'r')
    pg_curs.execute(sqlfile.read())
    table_data = pg_curs.fetchall()


    for num, row in enumerate(table_data):
      statsd.gauge('pgsql.bloat.index.real_size' , row[4] , tags=["pgsql_bloat_index_database:" + row[0], "pgsql_bloat_index_schema:" + row[1], "pgsql_bloat_index_tablename:" + row[2], "pgsql_bloat_index_indexname:" + row[3], "pgsql_bloat_index:pgsql.bloat.index", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.index.fillfactor' , row[5] , tags=["pgsql_bloat_index_database:" + row[0], "pgsql_bloat_index_schema:" + row[1], "pgsql_bloat_index_tablename:" + row[2], "pgsql_bloat_index_indexname:" + row[3], "pgsql_bloat_index:pgsql.bloat.index", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.index.bloat_size' , row[6] , tags=["pgsql_bloat_index_database:" + row[0], "pgsql_bloat_index_schema:" + row[1], "pgsql_bloat_index_tablename:" + row[2], "pgsql_bloat_index_indexname:" + row[3], "pgsql_bloat_index:pgsql.bloat.index", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.index.bloat_ratio' , row[7] , tags=["pgsql_bloat_index_database:" + row[0], "pgsql_bloat_index_schema:" + row[1], "pgsql_bloat_index_tablename:" + row[2], "pgsql_bloat_index_indexname:" + row[3], "pgsql_bloat_index:pgsql.bloat.index", "dbinstanceidentifier:" + rds_identifier ] )
      statsd.gauge('pgsql.bloat.index.hot_upd_rate' , row[8] , tags=["pgsql_bloat_index_database:" + row[0], "pgsql_bloat_index_schema:" + row[1], "pgsql_bloat_index_tablename:" + row[2], "pgsql_bloat_index_indexname:" + row[3], "pgsql_bloat_index:pgsql.bloat.index", "dbinstanceidentifier:" + rds_identifier ] )
      #print ("row: ", row)
      #print (type(row))

    pg_curs.close()
    pg_conn.close()

if sys.argv[1] == 'RDS':
  rds_list_cmd = 'aws rds describe-db-instances --filters Name=engine,Values=postgres --profile antenor | grep \\"DBInstanceIdentifier\\" | awk \'{print $2}\' | sed \'s/"//g\' | sed \'s/,//g\' '
  rds_list = Popen(rds_list_cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for rds in rds_list.stdout:
    rds_endpoint = check_output(['aws rds describe-db-instances --db-instance-identifier ' + rds.strip() + ' --profile antenor | grep \\"Address\\" | awk \'{print $2}\' | sed \'s/"//g\' | sed \'s/,//g\' '],  shell=True)
    rds_flag = check_output(['grep ' + rds_endpoint.strip() + ' .pgpass | grep datadog | wc -l'],  shell=True)

    if (rds_flag.strip() == "1"):
      table_bloat_stats(rds_endpoint.strip(), rds.strip())
      index_bloat_stats(rds_endpoint.strip(), rds.strip())

else:
  #os.uname()[1]
  table_bloat_stats('localhost','')
  index_bloat_stats('localhost','')

