import fabric
import time
import sys
import re
import os
import shutil
import datetime
from datadog import initialize, statsd
from subprocess import Popen, PIPE, STDOUT

options = {
    'statsd_host':'127.0.0.1',
    'statsd_port':8125
}

initialize(**options)


def dmesg_timeout(host):
  cmd = 'if [[ -z "$(dmesg -T | grep "nvme.*timeout" | tail -n1 | cut -c 2-25)" || $(date -d "$(dmesg -T | grep "nvme.*timeout" | tail -n1 | cut -c 2-25) 1 hour" +%s) < $(date -d now +%s) ]]; then echo 0; else echo $(date -d now +%s) - $(date -d "$(dmesg -T | grep "nvme.*timeout" | tail -n1 | cut -c 2-25)" +%s); fi | bc '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    value = line.rstrip()
  statsd.gauge('linux.dmesg_disk_timeout', value , tags=["dmesg:" +host])

def chef_run(host):
  cmd = 'if [ -e /lendico-logs/chef/client.log ]; then tail -n -20 /lendico-logs/chef/client.log | grep "Chef run process exited unsuccessfully" | awk "END { if (NR) print 0; else print 1 }"; elif  [ -e /lendico-logs/chef/client.log-$(date -d now +%Y%m%d) ]; then tail -n -20 /lendico-logs/chef/client.log-$(date -d now +%Y%m%d) | grep "Chef run process exited unsuccessfully" | awk "END { if (NR) print 0; else print 1 }"; else echo 1; fi | bc'
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    value = line.rstrip()
  statsd.gauge('linux.chef_run', value , tags=["chef_run:" +host])

def chef_checkin(host):
  cmd = 'if [ -s /lendico-logs/chef/client.log ]; then echo $(date -d now +%s) - $(date -d "$(tail -n1 /lendico-logs/chef/client.log | cut -c 2-25)" +%s); elif  [ -s /lendico-logs/chef/client.log-$(date -d now +%Y%m%d) ]; then echo $(date -d now +%s) - $(date -d "$(tail -n1 /lendico-logs/chef/client.log-$(date -d now +%Y%m%d) | cut -c 2-25)" +%s); else echo $(date -d now +%s) - $(date -d "$(tail -n1 /tmp/user_data_init.log | cut -c 2-25)" +%s); fi | bc'
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    value = line.rstrip()
  statsd.gauge('linux.chef_checkin', value , tags=["chef_checkin:" +host])

def md_degraded(host):
  cmd = 'ls /sys/class/block | grep md | awk \'BEGIN{printf ""} /md/ {printf c""$1"";c="\\n"}; END{print ""}\' '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    md = line.rstrip()
    cmd_line = 'cat /sys/block/' +md+ '/md/array_state | grep -c clean'
    script_line = Popen(cmd_line, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    for line in script_line.stdout:
      #statsd.gauge('linux.md_degraded_' +md, line.rstrip() , tags=["linux:md_degraded_" +md])
      statsd.gauge('linux.md_degraded', line.rstrip() , tags=["md_degraded:" +md])

def md_raid_disks(host):
  cmd = 'ls /sys/class/block | awk \'BEGIN{printf ""} /md/ {printf c""$1"";c="\\n"}; END{print ""}\' '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    md = line.rstrip()
    cmd_line = 'cat /sys/block/' +md+ '/md/raid_disks'
    script_line = Popen(cmd_line, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    for line in script_line.stdout:
      statsd.gauge('linux.md_raid_disks', line.rstrip() , tags=["md_raid_disks:" +md])

def fstab(host):
  cmd = 'mount -fa 2>&1 | wc -m '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('linux.fstab', line.rstrip() , tags=["fstab:" +host])

def process(host):
  cmd = 'ps auxr | wc -l'
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('linux.process_running', line.rstrip() , tags=["process_running:" +host])
  cmd = 'ps -e | wc -l'
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('linux.process', line.rstrip() , tags=["process:" +host])

def disk_iostat(host):
  cmd = '. /lendico/monitoring/disk_iostat/scripts/iostat-collect.sh /tmp/iostat_group.out 10 1 || echo 1'
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  disk_list = "df | grep md | awk '{print $6}' | sed 's/\///g'"
  script_disk_list = Popen(disk_list, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for disk in script_disk_list.stdout:
    metric_list = 'str1=" tps awsiops end" && str2=$(iostat -x 1 1 | grep Device | sed \'s/%//g\')$str1 && echo $str2 | awk -v f=1 -v t=1 \'{for(i=1;i<NF;i++) if(i>=f && i<=t) continue; else printf("%s%s \\n" ,$i,(i!=NF)?OFS:ORS)}\' '
    script_metric_list = Popen(metric_list, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    for metric in script_metric_list.stdout:
      metric_get = '. /lendico/monitoring/disk_iostat/scripts/iostat-parse.sh /tmp/iostat_group.out ' +disk.rstrip()+ ' "' +metric.rstrip()+ '"'
      script_metric_get = Popen(metric_get, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
      for metric_get in script_metric_get.stdout:
        statsd.gauge('linux.iostat.' +metric.replace('-','_').replace('/','_').rstrip() , metric_get.rstrip() , tags=["iostat_disk:" +disk.rstrip()] )

def check_process(host):
  cmd = 'sudo systemctl status postgresql-$(cat /lendico/postgresql/pg_version) | grep Active | awk \'{if($2 == "active") print 1; else print 0}\' '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('linux.status.postgres', line.rstrip() , tags=["status:postgres" ])

  cmd = 'sudo systemctl status pgbouncer | grep Active | awk \'{if($2 == "active") print 1; else print 0}\' '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('linux.status.pgbouncer', line.rstrip() , tags=["status:pgbouncer"])

  cmd = 'sudo systemctl status crond | grep "Active:" | head -n1 | awk \'{if($2 == "active") print 1; else print 0}\' '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('linux.status.crond', line.rstrip() , tags=["status:crond" ])

  cmd = 'ps ef -u postgres | grep wal*.sender | awk -v pg=$(cat /lendico/postgresql/pg_version) \'{ if(pg == "10") print $11; else print $9 }\' '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('pgsql.check.walsender', 1 , tags=["walsender:" +line.rstrip() ])

  cmd = 'ps ef -u postgres | grep wal*.receiver'
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('pgsql.check.walreceiver', 1 , tags=["walreceiver:" +host ])

  cmd = 'ps ef -u postgres | grep "logical replication worker" | awk \'{print $13}\' '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('pgsql.check.logical_worker', 1 , tags=["subscription:" +line.rstrip() ])

  cmd = 'ps ef -u postgres | grep "logical replication launcher" '
  script = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
  for line in script.stdout:
    statsd.gauge('pgsql.check.logical_launcher', 1 , tags=["logical_launcher:" +host ])


check_process(os.uname()[1])
dmesg_timeout(os.uname()[1])
#chef_run(os.uname()[1])
#chef_checkin(os.uname()[1])
md_degraded(os.uname()[1])
md_raid_disks(os.uname()[1])
fstab(os.uname()[1])
process(os.uname()[1])
disk_iostat(os.uname()[1])

