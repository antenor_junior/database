#/bin/bash
# ex: bash /pgbadger/log/lendico-postgres/tar.sh "/pgbadger/log/sandbox-postgres-final"
 
set -ex
DIR=$1
DIR2=$(echo $DIR | sed 's/\///')
for file in $(ls $DIR/*.tar.gz)
do
  tar -vxf $file
  mv $DIR2/* $DIR
  rm -rf $DIR2
  rm -rf $file 
done
