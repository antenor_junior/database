#!\bin\bash



#for i in $(seq 0 180);
#do
  #echo $i
  #i2=$(echo $i'+1' | bc)
  #psql -h lendico-workflow.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -U root -d postgres -c " select now()::date- interval '$i day' , now()::date - interval '$i2 day'  "
  #exit 0
  psql -h lendico-workflow.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -U root -d lendico_workflow -c "
  set role ulendico_workflow;

  insert into analise.cpf
select  data,
        sum(case when erros > 0 then 1 else 0 end) as cpf_com_erro,
        avg(estados) tentativas_por_cpf,
        sum(case when erros > 0 and sucessos > 0 then 1 else 0 end) as sucesso_apos_erro,
        count(distinct cpfs) cpfs_unicos
                
from (
         select date_trunc('day', process.created_at) as data,
                ps1.result -> 'activities' -> 0 -> 'data' ->> 'cpf'                                         as cpfs,
                count(distinct process_state.id)                                                            as estados,
                sum(case when process_state.result ->> 'status' = '400' then 1 else 0 end)                  as erros,
                sum(case when process_state.result ->> 'status' in ('200', '201', '206') then 1 else 0 end) as sucessos
         from workflow
                join process on workflow.id = process.workflow_id
                                  AND process.created_at >= now()::date- interval '1 day' 
                                  and process.created_at < now()::date- interval '0 day' 
                  and workflow.name in ('ACQ_REQUEST')
                join process_state on process.id = process_state.process_id and process_state.node_id = 's2'
                join process_state ps1 on process_state.process_id = ps1.process_id
                  and ps1.node_id = 'u0' and ps1.next_node_id = 'f0'
         where 1=1
           
         group by 1,2
     ) as base
group by 1;
  "
 #echo 'sleeping 5s'
 #time sleep 5 
#done
