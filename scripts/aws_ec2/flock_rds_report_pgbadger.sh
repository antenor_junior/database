#!/bin/bash

#set -e

echo '================================================='
if [ -f "/tmp/flock_rds_report_pgbadger.lock" ]; then
  echo "still in progress - $(date)"
else
  touch /tmp/flock_rds_report_pgbadger.lock
  bash /pgbadger/rds_report_pgbadger.sh
  #ls -lha /tmp/flock_rds_report_pgbadger.lock
  rm -f /tmp/flock_rds_report_pgbadger.lock
fi
echo '================================================='
