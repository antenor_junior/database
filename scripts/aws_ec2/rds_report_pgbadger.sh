sudo vim /etc/cron.d/rds_report_pgbadger
30 01 * * * centos bash /pgbadger/flock_rds_report_pgbadger.sh 2>&1 | tee -a /tmp/rds_report_pgbadger.log

vim /pgbadger/rds_report_pgbadger.sh

#!/bin/bash

#set -e 
DIR_PGBADGER=/pgbadger/reports/
DIR_PGBADGER_LOG=/pgbadger/log/

mkdir -p $DIR_PGBADGER_LOG
mkdir -p $DIR_PGBADGER
  
rm -rf ${DIR_PGBADGER}/index.html
cat  /pgbadger/rds_list > ${DIR_PGBADGER}/lista_rds.html

BUCKET_S3="lendico-database/postgres/pgbadger/reports"
echo '================================================='
for INSTANCE_NAME in $(cat /pgbadger/rds_list)
do
  
  DIR_PGBADGER=/pgbadger/reports/$INSTANCE_NAME
  DIR_PGBADGER_LOG=/pgbadger/log/$INSTANCE_NAME
  mkdir -p $DIR_PGBADGER_LOG
  mkdir -p $DIR_PGBADGER
  
  for LOG_FILE in $(ls -l $DIR_PGBADGER_LOG | grep postgresql | awk {'print $9'})
  do
  
    echo "Generating general report"

    rm -f $DIR_PGBADGER/pgbadger.pid
	CPU=$(nproc --all)
    #if [ "$INSTANCE_NAME" != "lendico-workflow" ]; then
      pgbadger -v -t 100 -j $CPU --exclude-query "^(COPY)" --prefix '%t:%r:%u@%d:[%p]:' --pid-dir ${DIR_PGBADGER} $DIR_PGBADGER_LOG/$LOG_FILE -I -O $DIR_PGBADGER;
    #fi

    tar -cvzf $DIR_PGBADGER_LOG/$LOG_FILE.tar.gz $DIR_PGBADGER_LOG/$LOG_FILE
    rm -f $DIR_PGBADGER_LOG/$LOG_FILE
    mv $DIR_PGBADGER_LOG/$LOG_FILE.tar.gz /$DIR_PGBADGER_LOG/processed

  done

  echo "Sending general report - ${INSTANCE_NAME}"
  aws s3 sync ${DIR_PGBADGER} s3://$BUCKET_S3/$INSTANCE_NAME
  echo "Process finished for instance '${INSTANCE_NAME}' "

  #generate index pgbadger
  echo "Generating RDS databases report list"
  echo "<a href='http://lendico-database.s3-website-us-east-1.amazonaws.com/postgres/pgbadger/reports/${INSTANCE_NAME}/index.html' target='new'> ${INSTANCE_NAME} </a><br>" >> /pgbadger/reports/index.html

done

aws s3 cp /pgbadger/reports/index.html s3://$BUCKET_S3/
echo " "
echo "Uploading new lists to S3:"
aws s3 cp "/pgbadger/lista_rds.html" s3://$BUCKET_S3/
echo '================================================='

