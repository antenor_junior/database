#!/bin/bash

set -e

AWS_PROFILE="--profile default"
if [ $1 ]; then
  AWS_PROFILE="--profile "$1
fi

AWS_PROFILE=$(echo $AWS_PROFILE | sed 's/--profile //')

# This program requires that you supply these AWS environment variables.
# Can possibly pull this out of an AWS config in the future.
export AWS_DEFAULT_REGION=$(less .aws/config | grep $AWS_PROFILE -A 2 | grep region | awk {'print $3'})
export AWS_ACCESS_KEY_ID=$(less .aws/credentials | grep $AWS_PROFILE -A 2 | grep aws_access_key_id  | awk {'print $3'})
export AWS_SECRET_ACCESS_KEY=$(less .aws/credentials | grep $AWS_PROFILE -A 2 | grep aws_secret_access_key | awk {'print $3'})

echo '================================================='

mkdir -p /pgbadger/s3_coresystem/

if [ -z $2 ]; then
  DATE=$(date +%d%m%y)
else
  DATE=$2
fi
echo $AWS_PROFILE ' - ' $DATE

aws s3 cp s3://coresystem-legacy/LendicoPagamentosLegado/Pagamentos$DATE.csv /pgbadger/s3_coresystem/ --profile $AWS_PROFILE
mv /pgbadger/s3_coresystem/Pagamentos$DATE.csv /pgbadger/s3_coresystem/payment_coresystem.csv
mysqlimport --defaults-group-suffix=2 -vP 3306 -h lendico-prod.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -u root --fields-terminated-by="," --lines-terminated-by="\r\n" --local lendico-payments /pgbadger/s3_coresystem/payment_coresystem.csv
mv /pgbadger/s3_coresystem/payment_coresystem.csv /pgbadger/s3_coresystem/Pagamentos$DATE.csv
echo '================================================='
