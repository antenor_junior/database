#arquivo de credencial de acesso ao ftp
echo "HOST 40.121.183.150" > .ftp
echo "USER lendico-ccbs" >> .ftp
echo "PASSWD ***" >> .ftp

sudo chmod 600 .ftp

#arquivo cron
sudo vim /etc/cron.d/ccb_export_ftp
0 4 * * * centos bash /ftp/ftp_export.sh 2>&1 | tee -a /tmp/ccb_ftp_export.log



#!/bin/bash

echo '======================================================================='
echo $(date)

DATE=$( date -d yesterday +%F)
QUERY_CP="SELECT
    CONCAT_WS (\";\", lac.partner_contract_id ,contr.number ) as 'NumContrato;NumCCB'
FROM \`lendico-contract-manager\`.contract AS contr 
	LEFT OUTER JOIN \`lendico-payments\`.lending_accounts AS lac ON lac.id = contr.id 
WHERE contr.partner_contract_id IS NOT NULL"
#AND lac.lending_deposit_dt = DATE_FORMAT(now() - INTERVAL 1 DAY,'%Y-%m-%d')"
#AND lac.lending_deposit_dt <= DATE_FORMAT(now() - INTERVAL 1 DAY,'%Y-%m-%d') order by lac.partner_contract_id"

QUERY_CDC="select
    contract_number as CCB, partner_contract_id AS CONTRATO
from
    lendings
where
    status not in ('CANCELED_PROPOSAL')
--and integration_base_date::date = (now() - interval '1 day')::date
--and integration_base_date::date <= (now() - interval '1 day')::date
"

DATE="full"

mysql -h lendico-prod.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -P 3306 -u ccb_select -Bre "$QUERY_CP" > /ftp/cp_$DATE.csv
psql -qp 5432 -d cdc_backoffice -h lendico-postgres-replica.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -U cdc_select -c "$QUERY_CDC" -o /ftp/cdc_$DATE.csv --csv

chmod 775 /ftp/*.csv

#exit 0

HOST=$( cat ~/.ftp | grep HOST | awk '{print$2}')
USER=$( cat ~/.ftp | grep USER | awk '{print$2}')
PASSWD=$( cat ~/.ftp | grep PASSWD | awk '{print$2}')
FILE_CP="cp_$DATE.csv"
FILE_CDC="cdc_$DATE.csv"
REMOTEPATH='/'

ftp -in $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
debug
cd $REMOTEPATH
lcd /ftp/
mdelete *.csv
put $FILE_CP
put $FILE_CDC
 
quit
END_SCRIPT
