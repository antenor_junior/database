#pacotes necessários para o centos
sudo yum install go
go get -u github.com/jonstacks/aws/cmd/rds-logs-download-url

#list rds
echo "lendico-postgres-db" >> /pgbadger/rds_list

#cron
sudo vim /etc/cron.d/rds_get_log
30 00 * * * centos bash /pgbadger/rds_get_log.sh "antenor" 2>&1 | tee -a /tmp/rds_get_log.log

#script
vim /pgbadger/rds_get_log.sh

#!/bin/bash

set -e

AWS_PROFILE="--profile default"
if [ $1 ]; then
  AWS_PROFILE="--profile "$1
fi

aws rds describe-db-instances --filters Name=engine,Values=postgres $AWS_PROFILE | grep "\"DBInstanceIdentifier\"" | awk '{print $2}' | sed 's/"//g' | sed 's/,//g' > /pgbadger/rds_list

AWS_PROFILE=$(echo $AWS_PROFILE | sed 's/--profile //')

# This program requires that you supply these AWS environment variables.
# Can possibly pull this out of an AWS config in the future.
export AWS_DEFAULT_REGION=$(less .aws/config | grep $AWS_PROFILE -A 2 | grep region | awk {'print $3'})
export AWS_ACCESS_KEY_ID=$(less .aws/credentials | grep $AWS_PROFILE -A 2 | grep aws_access_key_id  | awk {'print $3'})
export AWS_SECRET_ACCESS_KEY=$(less .aws/credentials | grep $AWS_PROFILE -A 2 | grep aws_secret_access_key | awk {'print $3'})

echo '================================================='
for INSTANCE_NAME in $(cat /pgbadger/rds_list)
do
  
  mkdir -p /pgbadger/log/
  mkdir -p /pgbadger/log/$INSTANCE_NAME
  mkdir -p /pgbadger/log/$INSTANCE_NAME/processed
  
  if [ -z $2 ]; then
    LOG_NAME="error/postgresql.log."$(date -d yesterday +%F)
  else
    LOG_NAME="error/postgresql.log."$2
  fi
  echo $INSTANCE_NAME ' - ' $LOG_NAME
  LOG_URL=$(/home/centos/go/bin/rds-logs-download-url $INSTANCE_NAME $LOG_NAME)
  # -s for silent, -f for fail so we can retry on failure
  #echo "curl -f -o " $(basename $LOG_NAME) $LOG_URL
  curl -f -o "/pgbadger/log/$INSTANCE_NAME/"$(basename $LOG_NAME) $LOG_URL
  echo '================================================='
done
