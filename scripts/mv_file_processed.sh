#!/bin/bash
#back file processed on export_data or export_schema to re-executed again
#ex: bash $(find ~ -name mv_file_processed.sh) "pg_schema_jupiter" "/tmp"
DIR=$2

for file in $(ls $DIR/ | grep $1.*END)
do
	mv $DIR/$file $DIR/$( echo $file | sed 's/END/sql/') 
done
