#/bin/bash
#bash $(find ~ -name lendico_export_schema_only.sh) "8880" "MYSQL" "netuno" "" "--defaults-group-suffix=dev"  2>&1 | tee -a /tmp/lendico_export_schema_only.log
#PG MULTI SCHEMA -> bash $(find ~ -name lendico_export_schema_only.sh) "8880" "PG" "netuno" "MULTI" 2>&1 | tee -a /tmp/lendico_export_schema_only.log
#PG MULTI BASE -> bash $(find ~ -name lendico_export_schema_only.sh) "8880" "PG" "netuno" "" 2>&1 | tee -a /tmp/lendico_export_schema_only.log
set -e
PORT=$1
ENGINE=$2
PG_TYPE=$4
OPT=$5
PG_BIN=$(find /usr -path '*/bin/*' 2>&- | grep psql | sed 's/psql//' | tail -1)

if [ $ENGINE == "MYSQL" ]; then
  mysql $OPT -h 127.0.0.1 -P $PORT -u root -sBre "SELECT schema_name FROM information_schema.SCHEMATA WHERE  schema_name NOT IN ('Database','information_schema','innodb','mysql','performance_schema','sys','tmp') AND schema_name LIKE '$3%' ;" --ssl-mode=DISABLED > list_database.csv

else
  #psql -qtp $PORT -h localhost -d postgres -U root -c "SELECT D.datname || ';' || U.usename AS owner FROM pg_database D INNER JOIN pg_user U on U.usesysid=D.datdba WHERE D.datname NOT IN ('template0', 'template1', 'rdsadmin', 'postgres') AND D.datname LIKE '$3%'; " -o list_database.csv
  psql -qtp $PORT -h localhost -d postgres -U root -c "SELECT D.datname || ';' || R.rolname AS owner FROM pg_database D INNER JOIN pg_roles R on R.oid=D.datdba WHERE D.datname NOT IN ('template0', 'template1', 'rdsadmin', 'postgres') AND D.datname LIKE '$3%'; " -o list_database.csv
fi

for DB in $(cat list_database.csv)
do

  if [ $ENGINE == "MYSQL" ]; then
    echo "===================================="  
    echo "mysql schema from DB " $DB  
    echo "===================================="  
    mysqldump $OPT -dnfP $PORT -h 127.0.0.1 -u root --single-transaction --ssl-mode=DISABLED --column-statistics=0 --databases $DB > /tmp/mysql_schema_$DB.sql
    NEW_DB=$(echo $DB | sed 's/lendico-//' | sed 's/-sandbox//')
    sed -i 's/'$DB'/'$3'-'$NEW_DB'/' /tmp/mysql_schema_$DB.sql
  else
    OWNER=$( echo $DB | awk -F ";" '{print $2}')
    DB=$( echo $DB | awk -F ";" '{print $1}')
    echo "===================================="  
    echo "pg schema from DB " $DB  
    echo "===================================="  
    $PG_BIN/pg_dump -sxOh localhost -p $PORT -U root --no-comments --role=$OWNER -f /tmp/pg_schema_$DB.sql $DB
	if [ "$PG_TYPE" == "MULTI" ]; then
      NEW_SCHEMA=$(echo $DB | sed 's/'$3'-//' | sed 's/lendico-//' | sed 's/-sandbox//' | sed 's/-/_/g' | sed 's/lendico_//' | sed 's/_sandbox//')
      sed -i 's/public./'$NEW_SCHEMA'./g' /tmp/pg_schema_$DB.sql
	fi
    sed -i 's/OWNED/;--OWNED/g' /tmp/pg_schema_$DB.sql
  fi  

  echo "===================================="  
done

