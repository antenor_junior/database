#sudo vim lendico_dev_database_organization.sh

#!/bin/bash
#PG MULTI-SCHEMA -> bash $(find ~/lendico/git -name lendico_dev_database_organization.sh)  "8880" "PG" "MULTI" "terra" "tst"  "" 2>&1 | tee -a /tmp/lendico_dev_database_organization.log
#PG MULTI-BASE -> bash $(find ~/lendico/git -name lendico_dev_database_organization.sh) "8880" "PG" "" "terra" "tst"  "" 2>&1 | tee -a /tmp/lendico_dev_database_organization.log
#bash $(find ~/lendico/git -name lendico_dev_database_organization.sh) "8880" "MYSQL" "" "terra" "tst" " --defaults-group-suffix=dev "  2>&1 | tee -a /tmp/lendico_dev_database_organization.log
set -e

if [ -z "$2" ]; then
  echo "choose a engine DB to execute (PG or MYSQL) !!!"
  exit 0
elif [ -z "$4" -a -z "$5" ]; then
  echo "no SQUAD and DATABASE parameters passed, exit script !!!"
  exit 0
else
  ENGINE=$2
fi
PG_BIN=$(find /usr -path '*/bin/*' 2>&- | grep psql | sed 's/psql//' | tail -1)
PORT=$1
PG_TYPE=$3
SQUAD=$4
DATABASE=$5
OPT=$6

if [ -z $SQUAD ]; then
  SQUAD="jupiter;krypton;mercurio;plutao;titan;qa;hlg;netuno;terra"
fi

#SQUAD="qa"
ROLES="dml;ddl;app"
#MYSQL_DB="lendico"
PG_DB="bank-slip-services;fraud-flag-services;workflow;banking-services;campaign-manager;cashflow;cdc-backoffice;cdc-engine;cdc-manager;cdc-sorocred-services;history-analysis;inbound-api;score;metabase;serasa-services;metrics-publisher;notifier;outbound-api;sorocred-services;recupera-services"
MYSQL_DB="lendico;api;approval;backoffice;communication;contract-manager;lists;payments;pricing;serasa-services;users;user-communication;user-profile;blacklist;bpc-leads;cbss-services;cities;claris;plus;referrals;users"

if [ $ENGINE == "MYSQL" ]; then
  DB=$MYSQL_DB
else
  DB=$PG_DB
fi

if [ $DATABASE ]; then
  DB=$DATABASE
fi

#condicional de confirmacao de execucao do script
#https://stackoverflow.com/questions/1885525/how-do-i-prompt-a-user-for-confirmation-in-bash-script
read -p "Serão criados os objetos SQUAD: [$SQUAD] e DBs: [$DB]. Continue (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes";;
  n|N ) echo "no" && exit 0;;
  * ) echo "invalid" && exit 0;;
esac

function pause(){
   read -p "$*"
}

SQUAD_QTD=1;

#loop SQUAD
while [ $SQUAD_QTD -le $( echo $SQUAD | awk -F ";" '{print NF}') ];
do
  DB_QTD=1;
  DB_ROLE_QTD=1;
  echo '================================================='
  #loop create ROLES
  if [ $ENGINE == "MYSQL" ]; then
  
    while [ $DB_ROLE_QTD -le $( echo $ROLES | awk -F ";" '{print NF}') ];
    do
      DB_ROLE=$( echo $SQUAD | awk -F ";" '{print $'$SQUAD_QTD'}')"-"$( echo $ROLES | awk -F ";" '{print $'$DB_ROLE_QTD'}')
      echo "create user " $DB_ROLE  " on " $ENGINE
      FLAG_USER=$(mysql $OPT -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -se "select count(*) from mysql.user where user='$DB_ROLE'")
      if [ $FLAG_USER = 0 ]; then
        mysql $OPT -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -ve "CREATE USER '$DB_ROLE'@'%' IDENTIFIED BY '$DB_ROLE';"
      else
        echo "user exists - nothing to do"
      fi
      let DB_ROLE_QTD=DB_ROLE_QTD+1
      #pause 'Press [Enter] key to continue...'
    done
  fi
  
  #loop create DB
  while [ $DB_QTD -le $( echo $DB | awk -F ";" '{print NF}') ];
  do
    DB_ROLE_QTD=1
    DB_NAME=$( echo $SQUAD | awk -F ";" '{print $'$SQUAD_QTD'}')"-"$( echo $DB | awk -F ";" '{print $'$DB_QTD'}')
    echo "### create SQUAD "$DB_NAME " on " $ENGINE
	
    if [ $ENGINE == "MYSQL" ]; then
      mysql $OPT  -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -ve "CREATE DATABASE IF NOT EXISTS \`$DB_NAME\` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
      #loop GRANT permissions on ROLES
      while [ $DB_ROLE_QTD -le $( echo $ROLES | awk -F ";" '{print NF}') ];
      do
        ROLE=$( echo $ROLES | awk -F ";" '{print $'$DB_ROLE_QTD'}')
        DB_ROLE=$( echo $SQUAD | awk -F ";" '{print $'$SQUAD_QTD'}')"-"$ROLE
        echo "create permission to role " $DB_ROLE " on SQUAD " $DB_NAME  " on " $ENGINE
	    
        if [ $ROLE = "ddl" ]; then
          mysql $OPT -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -D $DB_NAME -ve "GRANT SELECT,INSERT,UPDATE,DELETE,INDEX,ALTER,CREATE,DROP,CREATE VIEW, TRIGGER, REFERENCES  ON \`$DB_NAME\`.* TO '$DB_ROLE'@'%' ;"
        else
          mysql $OPT -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -D $DB_NAME -ve "GRANT SELECT,INSERT,UPDATE,DELETE,INDEX ON \`$DB_NAME\`.* TO '$DB_ROLE'@'%' ;"
	    
        fi
        let DB_ROLE_QTD=DB_ROLE_QTD+1
      done
    else
      DB_NAME=$( echo $SQUAD | awk -F ";" '{print $'$SQUAD_QTD'}')
      SCHEMA_NAME=$( echo $DB | awk -F ";" '{print $'$DB_QTD'}')
      if [ "$PG_TYPE" == "MULTI" ]; then
        $PG_BIN/psql -p $PORT -h localhost -U root -d postgres -v schema_name=$SCHEMA_NAME -v service_name=$DB_NAME -v db_name=$DB_NAME -v db_collation="" -f $(find ~ -name create_service_db_multi_schema.sql)
      else
        SERVICE_NAME=$DB_NAME
        DB_NAME=$SERVICE_NAME"-"$( echo $DB | awk -F ";" '{print $'$DB_QTD'}')
        #DB_NAME=$( echo $SQUAD | awk -F ";" '{print $'$DB_QTD'}')"-"$( echo $DB | awk -F ";" '{print $'$DB_QTD'}')
        $PG_BIN/psql -p $PORT -h localhost -U root -d postgres -v service_name=$SERVICE_NAME -v db_name=$DB_NAME -v db_collation="" -f $(find ~ -name create_service_db.sql)
      fi
	  
	  fi

    echo '================================================='
    let DB_QTD=DB_QTD+1
  done

  echo '================================================='
  let SQUAD_QTD=SQUAD_QTD+1
done

