#/bin/bash
#bash $(find ~ -name lendico_import_schema_database.sh) "8880" "MYSQL" "plutao" "netuno" "--defaults-group-suffix=dev" 2>&1 | tee -a /tmp/lendico_import_schema_database.log
#bash $(find ~ -name lendico_import_schema_database.sh) "8880" "PG" "plutao" "netuno"  ""2>&1 | tee -a /tmp/lendico_import_schema_database.log
#PG MULTI SCHEMA -> bash $(find ~ -name lendico_import_schema_database.sh) "8880" "PG" "plutao" "netuno" "MULTI" 2>&1 | tee -a /tmp/lendico_import_schema_database.log
#PG MULTI BASE -> bash $(find ~ -name lendico_import_schema_database.sh) "8880" "PG" "plutao" "netuno" "" 2>&1 | tee -a /tmp/lendico_import_schema_database.log
set -e
PORT=$1
ENGINE=$2
SQUAD_DB_DEST=$3
SQUAD_DB_ORI=$4
PG_TYPE=$5
OPT=$6
PG_BIN=$(find /usr -path '*/bin/*' 2>&- | grep pg_restore | sed 's/pg_restore//' | tail -1)

#ls /tmp/ | grep "pg_schema" | sed 's/pg_schema_//' | sed 's/lendico_//' | sed 's/lendico-//' | sed 's/_sandbox.sql//' | sed 's/-sandbox.sql//'
if [ $ENGINE == "MYSQL" ]; then

  for SCRIPT_SCHEMA in $(ls /tmp | grep mysql_schema.*sql)
  do
    NEW_DATABASE=$SQUAD_DB_DEST"-"$( echo $SCRIPT_SCHEMA | grep "mysql_schema" | sed 's/mysql_schema_'$SQUAD_DB_ORI'//' | sed 's/mysql_schema_//'  | sed 's/lendico-//' | sed 's/-sandbox.sql//' | sed 's/.sql//')
    NEW_DATABASE=$(echo $NEW_DATABASE | sed 's/-$//')
    echo "===================================="  
    echo "mysql schema script '" $SCRIPT_SCHEMA "' on DB '" $NEW_DATABASE  "'"
    echo "===================================="  
	  sed -i 's/USE `/USE `'$NEW_DATABASE'`; -- /' /tmp/$SCRIPT_SCHEMA
    FLAG_DB=$(mysql $OPT -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -se "select count(*) from SCHEMATA where schema_name='$NEW_DATABASE'" information_schema)
    if [ $FLAG_DB -gt 0 ]; then	
      mysql $OPT -u root -h 127.0.0.1 -P $PORT  --ssl-mode=DISABLED $NEW_DATABASE  < /tmp/$SCRIPT_SCHEMA
      mv /tmp/$SCRIPT_SCHEMA /tmp/$( echo $SCRIPT_SCHEMA | sed 's/\.sql/\.END/')
    fi
  
  done

else

  for SCRIPT_SCHEMA in $(ls /tmp | grep pg_schema.*sql)
  do
    echo "===================================="  
    echo "pg schema script '" $SCRIPT_SCHEMA "' on DB '" $SQUAD_DB_DEST  "'"
    echo "===================================="  
    sed -i 's/regexp/\'$'\n/g' /tmp/$SCRIPT_SCHEMA
    sed -i 's/CREATE SCHEMA/--CREATE SCHEMA/' /tmp/$SCRIPT_SCHEMA
    sed -i '1s/^/SET ROLE '$OWNER'; --/' /tmp/$SCRIPT_SCHEMA

    if [ "$PG_TYPE" == "MULTI" ]; then
      #$PG_BIN/pg_restore -vh localhost -p $PORT -U root -d $SQUAD_DB_DEST --role=$SQUAD_DB_DEST -Fc /tmp/$SCRIPT_SCHEMA
      $PG_BIN/psql -h localhost -p $PORT -U root -d $SQUAD_DB_DEST -f /tmp/$SCRIPT_SCHEMA
    else
      NEW_DATABASE=$SQUAD_DB_DEST"-"$( echo $SCRIPT_SCHEMA | sed 's/pg_schema_'$SQUAD_DB_ORI'//' | sed 's/-sandbox.sql//' | sed 's/.sql//' | sed 's/_/-/' ) 
      NEW_DATABASE=$(echo $NEW_DATABASE | sed 's/-$//')
      echo "new DB '" $NEW_DATABASE  "'"

      OWNER=$($PG_BIN/psql -qtp $PORT -h localhost -d postgres -U root -c "SELECT R.rolname AS owner FROM pg_database D INNER JOIN pg_roles R on R.oid=D.datdba WHERE D.datname = '$NEW_DATABASE' ; " | sed '/^[[:space:]]*$/d' | sed 's/ //' )
      sed -i '1s/^/SET ROLE '$OWNER'; --/' /tmp/$SCRIPT_SCHEMA

      $PG_BIN/psql -h localhost -p $PORT -U root -d $NEW_DATABASE -f /tmp/$SCRIPT_SCHEMA
    fi
	mv /tmp/$SCRIPT_SCHEMA /tmp/$( echo $SCRIPT_SCHEMA | sed 's/\.sql/\.END/')
    
  done

fi

