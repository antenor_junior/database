#/bin/bash
#bash $(find ~ -name lendico_import_data_database.sh) "8880" "MYSQL" "plutao" "netuno" "" "" " --defaults-group-suffix=dev " 2>&1 | tee -a /tmp/lendico_import_data_database.log
#PG MULTI SCHEMA -> bash $(find ~ -name lendico_import_data_database.sh) "8880" "PG" "plutao" "netuno" "MULTI" 2>&1 | tee -a /tmp/lendico_import_data_database.log
#PG MULTI BASE -> bash $(find ~ -name lendico_import_data_database.sh) "8880" "PG" "plutao" "netuno" "" 2>&1 | tee -a /tmp/lendico_import_data_database.log
#set -e
PORT=$1
ENGINE=$2
SQUAD_DB_DEST=$3
SQUAD_DB_ORI=$4
PG_TYPE=$5
RESTORE_TYPE=$6
OPT=$7
PG_BIN=$(find /usr -path '*/bin/*' 2>&- | grep pg_restore | sed 's/pg_restore//' | tail -1)

#ls /tmp/ | grep "pg_data" | sed 's/pg_data_//' | sed 's/lendico_//' | sed 's/lendico-//' | sed 's/_sandbox.sql//' | sed 's/-sandbox.sql//'
if [ $ENGINE == "MYSQL" ]; then

  for SCRIPT_DATA in $(ls /tmp/ | grep mysql_data_$SQUAD_DB_ORI.*sql)
  do
    NEW_DATABASE=$SQUAD_DB_DEST"-"$( echo $SCRIPT_DATA | grep "mysql_data" | sed 's/mysql_data_'$SQUAD_DB_ORI'//' | sed 's/mysql_data_//'  | sed 's/lendico-//' | sed 's/-sandbox.sql//' | sed 's/.sql//')
    echo "===================================="  
    echo "mysql data script '" $SCRIPT_DATA "' on DB '" $NEW_DATABASE  "'"
    echo "===================================="  
	sed -i 's/USE `/USE `'$NEW_DATABASE'`; -- /' /tmp/$SCRIPT_DATA
	#exit 0
    FLAG_DB=$(mysql $OPT -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -se "select count(*) from SCHEMATA where schema_name='$NEW_DATABASE'" information_schema)
    if [ $FLAG_DB -gt 0 ]; then	
      mysql $OPT -u root -h 127.0.0.1 -P $PORT  --ssl-mode=DISABLED $NEW_DATABASE  < /tmp/$SCRIPT_DATA
      mv /tmp/$SCRIPT_DATA /tmp/$( echo $SCRIPT_DATA | sed 's/\.sql/\.END/')
    else
      echo "DB not exists "
    fi
  
  done

else

  for SCRIPT_DATA in $(ls /tmp/ | grep pg_data_$SQUAD_DB_ORI.*sql)
  do
    echo "===================================="  
    echo "pg data script '" $SCRIPT_DATA "' on DB '" $SQUAD_DB_DEST  "'"
    echo "===================================="  

    if [ $RESTORE_TYPE == "RESTORE" ]; then
      if [ $PG_TYPE == "MULTI" ]; then
        $PG_BIN/pg_restore -vh localhost -p $PORT -U root -d $SQUAD_DB_DEST --role=$SQUAD_DB_DEST -Fc /tmp/$SCRIPT_DATA $OPT
      else
        NEW_DATABASE=$SQUAD_DB_DEST"-"$( echo $SCRIPT_DATA | sed 's/pg_data_'$SQUAD_DB_ORI'//' | sed 's/-sandbox.sql//' | sed 's/.sql//' | sed 's/.dump//' | sed 's/_/-/')
        NEW_DATABASE=$(echo $NEW_DATABASE | sed 's/-$//')
        echo "new DB '" $NEW_DATABASE  "'"
        $PG_BIN/pg_restore -vh localhost -p $PORT -U root -d $NEW_DATABASE --role=$SQUAD_DB_DEST -Fc /tmp/$SCRIPT_DATA  $OPT
      fi
	
	else
      sed -i 's/regexp/\'$'\n/g' /tmp/$SCRIPT_DATA
      sed -i '1s/^/SET ROLE '$SQUAD_DB_DEST'; --/' /tmp/$SCRIPT_DATA
      sed -i 's/CREATE SCHEMA/--CREATE SCHEMA/' /tmp/$SCRIPT_DATA
      if [ "$PG_TYPE" == "MULTI" ]; then
        #$PG_BIN/pg_restore -vh localhost -p $PORT -U root -d $SQUAD_DB_DEST --role=$SQUAD_DB_DEST -Fc /tmp/$SCRIPT_DATA
        $PG_BIN/psql -h localhost -p $PORT -U root -d $SQUAD_DB_DEST -f /tmp/$SCRIPT_DATA
      else
        NEW_DATABASE=$SQUAD_DB_DEST"-"$( echo $SCRIPT_DATA | sed 's/pg_data_'$SQUAD_DB_ORI'//'  | sed 's/-sandbox.sql//' | sed 's/.sql//' | sed 's/_/-/')
        NEW_DATABASE=$(echo $NEW_DATABASE | sed 's/-$//')
        echo "new DB '" $NEW_DATABASE  "'"

        OWNER=$($PG_BIN/psql -qtp $PORT -h localhost -d postgres -U root -c "SELECT R.rolname AS owner FROM pg_database D INNER JOIN pg_roles R on R.oid=D.datdba WHERE D.datname = '$NEW_DATABASE' ; " | sed '/^[[:space:]]*$/d' | sed 's/ //' )
        sed -i '1s/^/SET ROLE '$OWNER'; --/' /tmp/$SCRIPT_SCHEMA

        $PG_BIN/psql -h localhost -p $PORT -U root -d $NEW_DATABASE -f /tmp/$SCRIPT_DATA
      fi
	fi
	mv /tmp/$SCRIPT_DATA /tmp/$( echo $SCRIPT_DATA | sed 's/\.sql/\.END/' | sed 's/\.dump/\.END/')
    
  done

fi

