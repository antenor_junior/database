#!/usr/bin/env bash
# Description:	Script for iostat monitoring
# Author:	Epikhin Mikhail michael@nomanlab.org
# Revision 1:	Lesovsky A.V. lesovsky@gmail.com

SECONDS=$2
TOFILE=$1
DISK_GROUP=$3
IOSTAT="/usr/bin/iostat"

[[ $# -lt 2 ]] && { echo "FATAL: some parameters not specified"; exit 1; }

if [ -z $DISK_GROUP ]; then
  DISK=$($IOSTAT -x 1 $SECONDS | awk 'BEGIN {check=0;} {if(check==1 && $1=="avg-cpu:"){check=0}if(check==1 && $1!=""){print $0}if($1=="Device:"){check=1}}' | tr '\n' '|')
else
  DISK_GROUP=$(cat /proc/mdstat | grep md | awk -v f=2 -v t=4 '{for(i=1;i<=NF;i++) if(i>=f && i<=t) continue; else printf("%s%s" ,$i,(i!=NF)?OFS:ORS)}' | sed 's/\[[[:digit:]]]//g' | while read -r device list; do mount_point="$( df "/dev/${device}" | tail -n1 | awk '{print $NF}' | sed 's/\///g' )"; echo " -g ${mount_point} ${list}"; done)
  DISK=$($IOSTAT -x $DISK_GROUP 1 $SECONDS  | awk 'BEGIN {check=0;} {if(check==1 && $1=="avg-cpu:"){check=0}if(check==1 && $1!=""){print $0}if($1=="Device:"){check=1}}' | tr '\n' '|')
fi

echo $DISK | sed 's/|/\n/g' > $TOFILE

#IFOOD - collect reference from AWS disk IOPS
cat /proc/mdstat | grep md | awk '{print $0}' | sed 's/\[[[:digit:]]]//g' | while read -r device list; do mount_point="$( df "/dev/${device}" | tail -n1 | awk '{print $1"|"$NF}' | sed 's/\/dev\///g' | sed 's/\///g' )"; echo "      ${mount_point} ${list}"; done | sed 's/label:\//label:/' | awk -v f=1 -v t=4 '{for(i=1;i<=NF;i++) if(i>=f && i<=t) continue; else printf("%s%s \n" ,$1 " " $i " /dev/"$i,(i!=NF)?OFS:ORS)}' | while read line; do iops=$(sudo -iu root fdisk -l | grep "$(echo  $line | awk '{print $3}')" | awk '{print $5}'); echo $line "" $iops; done | grep "/dev/" > /tmp/disk.out
echo 0
