

use lendico-contract-manager
contract

lendico-user-profile.profile



{"issuing_institution": "SSP", "issuing_state": "GO", "issue_dt": "1963-07-28", "number": "1572443"}


select cpf,approval_data
from contract where cpf='29107911149';

--===========================================
--MySQL > 5.6
--===========================================

select cpf, JSON_EXTRACT(approval_data, "$.issue_dt") 
from contract where cpf='29107911149';


select cpf, JSON_EXTRACT(approval_data, '$.issue_dt') 
from lendico-contract-manager.contract where cpf='29107911149';


--===========================================
--MySQL <= 5.6 (until 5.7 nor support json type)
--===========================================

select cpf, 

SUBSTRING_INDEX(
 SUBSTRING_INDEX(
  SUBSTRING_INDEX( approval_data, 'issue_dt', -1),
  '": ', -1),
 ' ', 1) AS issue_dt
 ,
 SUBSTRING_INDEX(
 SUBSTRING_INDEX(
  SUBSTRING_INDEX( approval_data, 'birth_dt', -1),
  '": ', -1),
 ' ', 1) AS birth_dt
 
		
from contract where cpf='29107911149';


---------------------------------------------------------
select cpf, 

SUBSTRING_INDEX(
      SUBSTRING_INDEX(
        SUBSTRING_INDEX(
          approval_data, 
          CONCAT(
            '"',
            SUBSTRING_INDEX(issue_dt,'$.', - 1),
            '"'
          ),
          - 1
        ),
        '",',
        1
      ),
      ':',
      - 1
    )
from contract where cpf='29107911149';

select cpf, JSON_KEYS(approval_data) 
from contract where cpf='29107911149';


-------
	1*SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(ap.input_data,
		'serasa_score": ',-1),
		',',1),
		'}',1) AS serasa_score
