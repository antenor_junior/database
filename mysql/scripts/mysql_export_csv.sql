--EXPORT CSV
mysql -h lendico-prod.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -P 3306 -u ccb_select -Bre "$QUERY_CP" > /ftp/cp_$DATE.csv

mysql -h 127.0.0.1 -P 8885 -u root -p -Bre "show databases;" --ssl-mode=DISABLED > mysql_db.csv


mysql -h 127.0.0.1 -P 8885 -u root -p -Bre 'SELECT table_schema "DB Name" FROM information_schema.tables GROUP BY table_schema HAVING ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) > 10 ;' --ssl-mode=DISABLED > mysql_db.csv

mysql -h 127.0.0.1 -P 8885 -u root -p -Bre 'SELECT * FROM mysql.slow_log WHERE start_time >= "2021-03-24 15:30" AND start_time <= "2021-03-24 15:50" ;' --ssl-mode=DISABLED > mysql_slow_log_1230-1250.csv
mysql -h 127.0.0.1 -P 8885 -u root -p -Bre 'SELECT * FROM mysql.general_log WHERE event_time >= "2021-03-24 15:30" AND event_time <= "2021-03-24 15:50" ;' --ssl-mode=DISABLED > mysql_general_log_1230-1250.csv

--DELIMITER | OUT FILE | ESCAPE TAB to ;
mysql -h 127.0.0.1 -P 8881 -u lgpd_full_ro -p -Bre "SELECT *  FROM `lendico-backoffice`.user_action WHERE created_dt >= '2021-05-01 00:00:00' and created_dt < '2021-06-01 00:00:00';" --skip-column-names --ssl-mode=DISABLED  lendico-backoffice | sed 's/\t/;/g' > back-0501-0601.csv

--skip-line-numbers

mysql -h 127.0.0.1 -P 8880 -u root -p -Bre 'SHOW ENGINE INNODB STATUS \G ;' --ssl-mode=DISABLED > mysql_innodb_status.csv


mysql -h 127.0.0.1 -P 8885 -u root -p -Bre 'SELECT * FROM `lendico-user-profile`.income_document_data_body WHERE created_dt >= "2021-06-17 19:30" ;' --ssl-mode=DISABLED > income_document_data_body_past1930.csv

--EXEC sql file AND EXPORT RESULT
mysql -h 127.0.0.1 -P 8880 -u root -p -Br  --ssl-mode=DISABLED < [SCRIPT_QUERY].sql > [ARQUIVO_SAIDA].csv

mysql -h 127.0.0.1 -P 8880 -u root -p -Br  --ssl-mode=DISABLED lendico-payments-mercurio < my.sql 


--EXPORT CSV (with function)
SET @TS = DATE_FORMAT(NOW(),'_%Y_%m_%d');

SET @FOLDER = '/ccb/';
SET @PREFIX = 'ccb';
SET @EXT    = '.csv';


SET @CMD = CONCAT("
SELECT
    lac.partner_contract_id AS 'numero_contrato',
    contr.number AS 'numero_ccb',
    lac.lending_deposit_dt AS 'data_aquisicao',
    lac.assigned_dt AS 'data_concessao'
FROM
    `lendico-payments`.lending_accounts AS lac
    INNER JOIN `lendico-contract-manager`.contract AS contr ON contr.id = lac.id
WHERE assignee_name = 'FIDC_MONZA'
AND lac.lending_deposit_dt = DATE_FORMAT(now() - INTERVAL 1 DAY,'%Y-%m-%d')
					INTO OUTFILE '",@FOLDER,@PREFIX,@TS,@EXT,
				   "' FIELDS ENCLOSED BY '\"' TERMINATED BY ';' ESCAPED BY '\"'",
				   "  LINES TERMINATED BY '\r\n';");

PREPARE statement FROM @CMD;

EXECUTE statement;


SELECT
    lac.partner_contract_id AS 'numero_contrato',
    contr.number AS 'numero_ccb',
    lac.lending_deposit_dt AS 'data_aquisicao',
    lac.assigned_dt AS 'data_concessao'
FROM
    `lendico-payments`.lending_accounts AS lac
    INNER JOIN `lendico-contract-manager`.contract AS contr ON contr.id = lac.id
WHERE assignee_name = 'FIDC_MONZA'
AND lac.lending_deposit_dt = DATE_FORMAT(now() - INTERVAL 1 DAY,'%Y-%m-%d')
--AND lac.lending_deposit_dt <= DATE_FORMAT(now() ,'%Y-%m-%d')
--ORDER BY lac.lending_deposit_dt;