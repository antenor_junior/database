#!/bin/bash

#sudo vim mysql_generate_series_date.sh
#bash $(find ~ -name mysql_generate_series_date.sh) "8880" "2018-01-01" "2018-10-01" 2>&1 | tee -a /tmp/mysql_generate_series_date.log
#create table tab (series datetime);
set -e

PORT=$1
DATE_INI=$2
DATE_END=$3

function pause(){
   read -p "$*"
}

#loop create DB
while [ $(date -d  "$DATE_INI" +%s) -le $(date -d  "$DATE_END" +%s) ];
do

  echo '================================================='

  echo "generate date " $DATE_INI
  D_END=$(date -d "$DATE_INI +1 month" +%F)
  time mysql -P $PORT -h 127.0.0.1 -u root --ssl-mode=DISABLED -ve "CALL generate_series('$DATE_INI 00:00:00', '$D_END 00:00:00', 'INTERVAL 1 MINUTE'); insert into tab select * from series_tmp;" tmp
  DATE_INI=$D_END
  echo 'sleeping 2 min ... ' $(date +%F" "%T)
  sleep 120

done  

