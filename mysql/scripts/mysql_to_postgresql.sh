--PG_CHAMELEON
https://pgchameleon.org/documents/usage.html#example
https://pypi.org/project/pg-chameleon/


--PGLOADER
https://www.digitalocean.com/community/tutorials/how-to-migrate-mysql-database-to-postgres-using-pgloader
https://access.crunchydata.com/documentation/pgloader/3.6.2/ref/mysql/
https://pgloader.readthedocs.io/en/latest/ref/mysql.html#mysql-schema-transformations
https://medium.com/@nathanwillson/converting-from-mysql-to-postgres-with-pgloader-for-heroku-b1212c6ad932

pgloader --logfile "/tmp/mysql_pg_lendico_backoffice_qa2.log" mysql://pgloader:pgloader@lendico-mysql-db.clecbdjbx49l.us-east-1.rds.amazonaws.com:3306/lendico-backoffice-qa postgresql://antenor.junior:123456@localhost:5432/lendico_backoffice_qa

LOAD DATABASE
  FROM mysql://pgloader:pgloader@lendico-mysql-db.clecbdjbx49l.us-east-1.rds.amazonaws.com:3306/lendico-communication-plutao
  INTO postgresql://antenor.junior:123456@localhost:5432/lendico-communication-plutao
ALTER TABLE NAMES MATCHING ~/./  SET SCHEMA 'lendico_communication_plutao';

pgloader --logfile "/tmp/mysql_pg_lendico_communication_plutao.log" lendico_communication_plutao.load
