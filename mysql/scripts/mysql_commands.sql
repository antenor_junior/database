--===============================================
-- MySQL Commands
--===============================================

--user
CREATE USER 'ccb_select'@'10.0.31.0' IDENTIFIED BY '!nwf>+d-H<jQN7t.';
CREATE USER 'ccb_select'@'%' IDENTIFIED BY '!nwf>+d-H<jQN7t.';
--permission
GRANT SELECT ON `lendico-payments`.lending_accounts TO 'ccb_select'@'10.0.31.0';
GRANT SELECT ON `lendico-contract-manager`.contract TO 'ccb_select'@'10.0.31.0';



CREATE USER 'pgloader'@'%' IDENTIFIED BY 'pgloader';
GRANT SELECT ON *.* TO 'pgloader'@'%';
FLUSH PRIVILEGES;




CREATE USER 'ccb_select'@'localhost' 

CREATE ROLE cdc_select LOGIN;
\password  ccb_select '!nwf>+d-H<jQN7t.';
set role ucdc_backoffice;
GRANT CONNECT ON DATABASE cdc_backoffice TO cdc_select;
GRANT SELECT ON lendings TO cdc_select;

--USER PASSWORD
SET PASSWORD FOR 'USER'@'%' = PASSWORD('new_password');


--MySQL tables statistics (size, engine, no. of rows etc.)
https://geeksww.com/tutorials/database_management_systems/mysql/administration/mysql_tables_statistics_size_engine_no_of_rows_etc.php
https://dev.mysql.com/doc/refman/8.0/en/information-schema-statistics-table.html

--LIST OBJECTS
--------------------------------------------------------------------------------------------------------------------------
SHOW TABLE STATUS [{FROM | IN} db_name] [LIKE 'pattern' | WHERE expr]
SHOW TABLE STATUS LIKE '%'

SHOW INDEX
  FROM tbl_name
  FROM db_name

SHOW STATUS WHERE variable_name = 'threads_connected';
SHOW VARIABLES LIKE 'max_connections';

SHOW TABLES
SELECT table_schema "DB Name", table_name FROM information_schema.tables WHERE table_schema = 'tmp'

SHOW DATABASES
select  schema_name FROM information_schema.SCHEMATA where schema_name like 'krypton%';

SHOW FUNCTION STATUS [LIKE 'pattern' | WHERE search_condition];
SHOW PROCEDURE STATUS [LIKE 'pattern' | WHERE search_condition];

show create database `plutao-communication`;

--Script DELETE TABLE
SELECT CONCAT('delete  from `', table_name, '`;') FROM information_schema.tables WHERE table_schema = 'lendico-communication-sandbox';
--------------------------------------------------------------------------------------------------------------------------



--EXPLAIN
https://dev.mysql.com/doc/refman/5.6/en/explain-extended.html
https://www.sitepoint.com/using-explain-to-write-better-mysql-queries/
https://dev.mysql.com/doc/refman/5.6/en/explain-output.html#explain-extra-information

EXPLAIN EXTENDED
[query]\G

OBS: \G para listar cada item em nova linha

--“Show Full Processlist;” Equivalent Of MySQL For PostgreSQL
mysqladmin -u bob -p -i 1 processlist
show full processlist;

SELECT * FROM INFORMATION_SCHEMA.PROCESSLIST WHERE COMMAND != 'Sleep';
--Shows all queries running for 5 seconds or more:
SELECT * FROM INFORMATION_SCHEMA.PROCESSLIST WHERE COMMAND != 'Sleep' AND TIME >= 5;
--Show all running UPDATEs:
SELECT * FROM INFORMATION_SCHEMA.PROCESSLIST WHERE COMMAND != 'Sleep' AND INFO LIKE '%UPDATE %';
--For full details see: http://dev.mysql.com/doc/refman/5.1/en/processlist-table.html

--CONNECTION STATE
SELECT command,state,count(*) FROM INFORMATION_SCHEMA.PROCESSLIST GROUP BY command,state;



--Create index
https://dev.mysql.com/doc/refman/5.6/en/create-index.html

CREATE INDEX [name] ON [table](colunms) 
DROP INDEX [name] ON [table]

--MySQL Indexing Best Practices
https://medium.com/@akhilmathew_/mysql-indexing-best-practices-779282b0995b

--Execute query command line
mysql -u root -p -h slavedb.mydomain.com mydb_production "select * from users;"



--Materialized view
https://qastack.com.br/dba/86790/best-way-to-create-a-materialized-view-in-mysql
https://fromdual.com/mysql-materialized-views
http://www.coding-dude.com/wp/databases/creating-mysql-materialized-views/



--Create table
https://dev.mysql.com/doc/refman/8.0/en/create-table.html


--Prepared Statement
https://dev.mysql.com/doc/refman/5.6/en/sql-prepared-statements.html
https://www.mysqltutorial.org/mysql-prepared-statement.aspx/


--DB size
SELECT table_schema "DB Name",
        ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) "DB Size in MB" 
FROM information_schema.tables 
WHERE table_schema = 'lendico_temp'
GROUP BY table_schema
ORDER BY 2 DESC; 

SELECT table_schema "DB Name",
        ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) "DB Size in MB" 
FROM information_schema.tables 
GROUP BY table_schema
HAVING ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) > 10
ORDER BY 2 DESC; 


--TAB size
SELECT table_schema "DB Name", table_name,
        ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) "DB Size in MB" 
FROM information_schema.tables 
WHERE table_schema = 'tmp'
GROUP BY table_schema, table_name
ORDER BY 3 DESC; 


SELECT table_schema "DB Name", table_name,
        ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) "DB Size in MB" 
FROM information_schema.tables 
WHERE table_name = 'email_message'
GROUP BY table_schema, table_name

--SCRIPT generate GRANTS os DB
SELECT distinct  CONCAT ("grant REFERENCES, CREATE VIEW, TRIGGER ON `",  SCHEMA_NAME, "`.* TO 'netuno-ddl'@'%'; ")   FROM information_schema.SCHEMATA  WHERE SCHEMA_NAME like 'netuno-%';

--MySQL DUMP
--https://serverfault.com/questions/912162/mysqldump-throws-unknown-table-column-statistics-in-information-schema-1109
--DATA and SCHEMA
mysqldump -vfP 8886 -h 127.0.0.1 -u root -p --single-transaction --ssl-mode=DISABLED --column-statistics=0 --default-character-set=utf8  --databases lendico-user-profile-qa lendico-approval-qa > dump.sql
--IGNORE a table
mysqldump -vfP 8886 -h 127.0.0.1 -u root -p --single-transaction --ssl-mode=DISABLED --column-statistics=0 --default-character-set=utf8 --ignore-table=lendico-communication-qa.sms_whitelist --ignore-table=lendico-communication-qa.email_attachment --databases lendico-communication-qa > lendico-communication-qa.sql 
--DATA only
mysqldump -vfntP 8886 -h 127.0.0.1 -u root -p --single-transaction --ssl-mode=DISABLED --column-statistics=0 --default-character-set=utf8 --ignore-table=lendico-communication-qa.sms_whitelist --ignore-table=lendico-communication-qa.email_attachment  --databases lendico-communication-qa > lendico-communication-qa.sql 
--SCHEMA only
mysqldump -vdfnP 8886 -h 127.0.0.1 -u root -p --single-transaction --ssl-mode=DISABLED --column-statistics=0 --default-character-set=utf8 --ignore-table=lendico-communication-qa.sms_whitelist --ignore-table=lendico-communication-qa.email_attachment  --databases lendico-communication-qa > lendico-communication-qa.sql 



SELECT *  FROM `lendico-backoffice`.user_action WHERE created_dt >= '2021-04-01 00:00:00' and created_dt < '2021-05-01 00:00:00'

--DATA only | TABLE especific
mysqldump -vfntP 8886 -h 127.0.0.1 -u root -p --single-transaction --ssl-mode=DISABLED --column-statistics=0 --default-character-set=utf8 plutao-backoffice credit_analysis_queue > plutao_backoffice-credit_analysis_queue.sql 


mysqldump -vfntP 8886 -h 127.0.0.1 -u root -p --single-transaction --ssl-mode=DISABLED --column-statistics=0 ---default-character-set=utf8 netuno-lists city > netuno_city.sql 

--DATA only | DATABASE especific  | TABLE especific | WHERE condition
mysqldump -vfntP 8881 -h 127.0.0.1 -u lgpd_full_ro -p --single-transaction --ssl-mode=DISABLED --column-statistics=0 --default-character-set=utf8 --databases lendico-backoffice --tables user_action --where="created_dt >= '2021-04-01 00:00:00' and created_dt < '2021-05-01 00:00:00'" --result_file back-0401-0501.csv

tar -vczf lendico_backoffice_qa.tar.gz lendico_backoffice_qa.sql


--PASS
vim .my.cnf 
[client]
user=ccb_select
password=654123


--IMPORT (file have same table name) -d = delete all rows
mysqlimport -vdP 8880 -h 127.0.0.1 -u root -p --fields-terminated-by=";" --lines-terminated-by="\r\n" --local --ssl-mode=DISABLED lendico-payments Downloads/payments_upd.csv

-- [-d] = delete all rows; [BASE] - database to import data (ex: lendico-payment); [TABLE_NAME] - file need the same name of destination table (ex: payment_coresystem.csv)
mysqlimport -vdP 8880 -h 127.0.0.1 -u [USER] --fields-terminated-by="," --lines-terminated-by="\r\n" --local --ssl-mode=DISABLED [BASE] [PASTA]/[TABLE_NAME].csv
--APPEND
mysqlimport -vP 8880 -h 127.0.0.1 -u root --fields-terminated-by="," --lines-terminated-by="\r\n" --local --ssl-mode=DISABLED lendico-payments-mercurio Downloads/payment_coresystem.txt



--IMPORT dump
mysql -vh 127.0.0.1 -P 8880 -u root -p -Br  --ssl-mode=DISABLED lendico-communication-sandbox < lendico-communication-qa.sql

--date format
--https://stackoverflow.com/questions/15396058/how-to-convert-varchar-to-datetime-format-in-mysql
--DATE_FORMAT(STR_TO_DATE('5/16/2011 20:14 PM', '%c/%e/%Y %H:%i'), '%Y-%m-%d %H:%m:%s')
select *,DATE_FORMAT(STR_TO_DATE(date, '%e/%c/%Y'), '%Y-%m-%d') as dt from payments_upd limit 5;


--COPY a table
create table payments_bkp_2021_03_12 as select *  from payments where lending_id in (select l_id from payments_upd);


--UPDATE
--https://stackoverflow.com/questions/27154265/mysql-update-values-based-on-subquery/27154640
BEGIN;

update payments P
inner join payments_upd PU on P.id=PU.p_id and P.lending_id=PU.l_id
set P.due_date=DATE_FORMAT(STR_TO_DATE(PU.new_date, '%e/%c/%Y'), '%Y-%m-%d') 



--show variable
SHOW VARIABLES LIKE "general_log%";
SHOW VARIABLES LIKE "%query_log%";
SHOW VARIABLES LIKE "%fast_index_creation%";


--monitoring via servidor EC2
--innotop -u [USER] -P [PORT] -h [RDS DNS] -p [PASSWORD] -d [UPDATE_INTERVAL]
--press ? to see all commands
--https://www.percona.com/blog/2013/10/14/innotop-real-time-advanced-investigation-tool-mysql/
innotop -u root -P 3306 -h lendico-prod.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -p NaIDw0CC2TcU41LGcVG -d 1

--mytop -u [USER] -P [PORT] -h [RDS DNS] -p [PASSWORD] -d [DATABASE]
--press ? to see all commands
--https://www.digitalocean.com/community/tutorials/how-to-use-mytop-to-monitor-mysql-performance
mytop -u root -P 3306 -h lendico-prod.cvmvjsjhqvz5.us-east-1.rds.amazonaws.com -p NaIDw0CC2TcU41LGcVG -d lendico-pricing

--list process to KILL from a DB

--only active process
SELECT CONCAT('KILL ',ID,';') FROM INFORMATION_SCHEMA.PROCESSLIST WHERE DB = '[DATABASE]' AND COMMAND != 'Sleep' AND user != 'root' ;
--all process (include sleep)
SELECT CONCAT('KILL ',ID,';') FROM INFORMATION_SCHEMA.PROCESSLIST WHERE DB = '[DATABASE]' AND user != 'root' ;

