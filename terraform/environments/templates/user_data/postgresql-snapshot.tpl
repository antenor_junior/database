#!/bin/bash

{
set -x

yum clean all
rm -rf /var/cache/yum

rpm -ivh https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/libreport-filesystem-2.1.11-43.el7.centos.x86_64.rpm
rpm -ivh https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/mdadm-4.1-1.el7.x86_64.rpm
yum install -y mdadm

volume_pgdata_count=${volume_pgdata_count}
volume_pgwal_count=${volume_pgwal_count}
volume_backup_count=${volume_backup_count}

ls -lh /dev/xvd*

## Get volumes/device information
awk_cmd='(NR > 1){printf(" ")} {printf("/dev/xvd%c", $1 + 97 + skip)} END{printf("\n")}'

volume_pgdata_devices="` seq 1 $${volume_pgdata_count} | awk -v skip=0 -v prefix='xvd' "$${awk_cmd}" `"
volume_pgwal_devices="` seq 1 $${volume_pgwal_count} | awk -v skip=$${volume_pgdata_count} -v prefix='xvd' "$${awk_cmd}" `"
volume_backup_devices="` seq 1 $${volume_backup_count} | awk -v skip=$(( $${volume_pgdata_count} + $${volume_pgwal_count} )) -v prefix='xvd' "$${awk_cmd}" `"


if [ -b /dev/xvda ]; then

    counter=0

    while ! (echo -n $${volume_pgdata_devices} $${volume_pgwal_devices} $${volume_backup_devices} | xargs -d' ' -L1 test -b); do
        sleep 30
        counter=$((counter + 1))
        echo "waiting devices to be available (retry $${counter}/10)"
        if [ $counter -ge 10 ]; then
        exit
        fi
    done
else
    rpm -ivh https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/nvme-cli-1.8.1-3.el7.x86_64.rpm
    yum install -y nvme-cli

    counter=0

    count_all="$(( $${volume_pgdata_count} + $${volume_pgwal_count} + $${volume_backup_count} ))"
    while ! [ "$( ls /dev/nvme*n1 | grep -vc nvme0n1 )" == "$${count_all}" ]; do
        sleep 30
        counter=$((counter + 1))
        echo "waiting devices to be available (retry $${counter}/10) - $( ls /dev/nvme*n1 | grep -vc nvme0n1 ) attached, $${count_all} expected"
        if [ $counter -ge 10 ]; then
        exit
        fi
    done
    echo OK

    function nvme_get_ebs_names() {
        for device in /dev/nvme*n1; do
            ebsname="$( nvme id-ctrl -v "$${device}" | grep '^0000:' | sed 's/.*"\(\/dev\/\)\?\(sd[^\.]*\).*"/\2/' )"
            echo "$${device} $${ebsname}"
        done
    }
    nvme2ebs="$( nvme_get_ebs_names )"

    function remap_to_ebs_device_name() {
        local origin="$1"
        echo "$${origin}" | tr ' ' '\n' | sed 's/\/dev\/xvd\(.*\)/sd\1/'
    }

    function remap_to_nvme_device() {
        local names="$1"
        for name in $${names}; do
            device="$( echo "$${nvme2ebs}" | awk -v name="$${name}" '$2 == name{print $1}' )"
            echo "$${name} is mapped to $${device}" >&2
            echo -n "$${device} "
        done
    }

    volume_pgdata_devices="$( remap_to_ebs_device_name "$${volume_pgdata_devices}" )"
    volume_pgwal_devices="$( remap_to_ebs_device_name "$${volume_pgwal_devices}" )"
    volume_backup_devices="$( remap_to_ebs_device_name "$${volume_backup_devices}" )"

    volume_pgdata_devices="$( remap_to_nvme_device "$${volume_pgdata_devices}" )"
    volume_pgwal_devices="$( remap_to_nvme_device "$${volume_pgwal_devices}" )"
    volume_backup_devices="$( remap_to_nvme_device "$${volume_backup_devices}" )"

    echo "pgdata: $${volume_pgdata_devices}"
    echo "pgwal: $${volume_pgwal_devices}"
    echo "backup: $${volume_backup_devices}"
fi


#yum install -y fio

## Mount volumes

mkdir -p /data /wal /backup

if ! ( grep -q 'LABEL=DATA' /etc/fstab); then
    echo 'LABEL=DATA /data ext4 defaults,nofail,noatime,nodiratime,errors=continue 0 0' >> /etc/fstab
fi

if [ ${volume_pgwal_count} -gt 0 ]; then
    mdadm --create /dev/md1 --level=0 --chunk=16K --raid-devices=${volume_pgwal_count} --force $${volume_pgwal_devices}
    mkfs.ext4 $${volume_pgwal_devices}
    /sbin/blockdev --setra 2048 /dev/md1
    e2label /dev/md1 WAL

    if ! ( grep -q 'LABEL=WAL' /etc/fstab); then
        echo 'LABEL=WAL /wal ext4 defaults,nofail,noatime,nodiratime,errors=remount-ro 0 0' >> /etc/fstab
    fi
fi

if [ ${volume_backup_count} -gt 0 ]; then
    mkfs.ext4 $${volume_backup_devices}
    e2label $${volume_backup_devices} BACKUP

    if ! ( grep -q 'LABEL=BACKUP' /etc/fstab); then
        echo 'LABEL=BACKUP /backup ext4 defaults,nofail,noatime,nodiratime,errors=remount-ro 0 0' >> /etc/fstab
    fi
fi

#echo "=== fsck ==="
#fsck -vy /dev/md0 2>&1 >> /tmp/user_data_init.log

## Install and run Chef

export SRV_NAME="${service_name}"
export ENVIRONMENT="${environment}"
export ROLE="${service_role_chef}"
export DOMAINNAME="dc.lendico.com.br"
curl -L https://chef.dc.lendico.com.br/shared/install_instance.sh | bash -x
chef-client -E $${ENVIRONMENT} -S https://chef.dc.lendico.com.br -j /etc/chef/first-boot.json

systemctl restart td-agent.service
systemctl stop datadog-agent
rm -rf /etc/cron.d/chef-client

systemctl stop postgresql-$(cat /lendico/postgresql/pg_version)
rm -rf /data/$(cat /lendico/postgresql/pg_version)

mdadm --create /dev/md0 --level=0 --chunk=16K --raid-devices=${volume_pgdata_count} --run --force $${volume_pgdata_devices}

## mkfs.ext4 $${volume_pgdata_devices}
/sbin/blockdev --setra 2048 /dev/md0
e2label /dev/md0 DATA


echo "=== mount ==="
mount --rw -va 2>&1 >> /tmp/user_data_init.log

rm -rf /data/$(cat /lendico/postgresql/pg_version)/pgdata/pg_replslot/*
rm -rf /data/$(cat /lendico/postgresql/pg_version)/pgdata/pg_xact/$( ls /data/$(cat /lendico/postgresql/pg_version)/pgdata/pg_xact | tail -n1)
rm -rf /data/$(cat /lendico/postgresql/pg_version)/pgdata/pg_wal/archive_status
rm -rf /data/$(cat /lendico/postgresql/pg_version)/pgdata/pg_wal/0*

echo "=== umount ==="
umount -v /data 2>&1 >> /tmp/user_data_init.log

echo "=== fsck ==="
fsck -vy /dev/md0 2>&1 >> /tmp/user_data_init.log

echo "=== mount ==="
mount --rw -va 2>&1 >> /tmp/user_data_init.log

} 2>&1 >> /tmp/user_data_init.log

