/* PG Bank Connector */
locals {
  config-pg-bank-connector = {
    service_name           = "pg-bank-connector"
    instance_type          = "t2.micro"
    #service_role_chef      = "pg-bank-connector"
    user_data              = "postgresql.tpl"
    iam_role               = "pg-bank-connector"
    volume_type            = "gp2"
    volume_size            = "50"
    ebs_optimized          = "false"
    # pgdata
    volume_pgdata_count    = "2"
    volume_pgdata_size     = "50"
    volume_pgdata_type     = "gp2"
    # pgwal
    volume_pgwal_count     = "2"
    volume_pgwal_size      = "50"
    volume_pgwal_type      = "gp2"

    disable_api_termination = "true"
    private_key_path       = "~/.ssh/terraform-dev"
    ssh_user               = "centos"
  }
  config-pg-bank-connector-ro = {
    ebs_optimized          = "false"
  }

  config-pg-bank-connector-dr = {
    ebs_optimized          = "false"
    # backup
    volume_backup_count    = "1"
    volume_backup_size     = "150"
    volume_backup_type     = "st1"
  }

}

#module "pg-bank-connector-iam-role" {
#  source      = "../../../../modules/aws/lendico-iam"
#  environment = "${var.environment}"
#  config      = "${local.config-pg-bank-connector}"
#  iam_role_dir     = "../../../../policies/iam_role"
#  iam_policies_dir = "../../../../policies/iam_policy"

#  region = "us-east-1"

#  custom_iam_policies = [  ]

#}

module "pg-bank-connector1" {
  source        = "../../../../modules/aws/lendico-postgresql"
  environment   = "${var.environment}"
  config        = "${local.config-pg-bank-connector}"
  subnet        = "lendico-private-us-east-1d"
  instance_name = "pg-bank-connector1"
  user_data_dir = "../../../../templates/user_data"

  region = "us-east-1"

  tags = {
    Service     = "postgres"
    #CostCenter  = "Billing"
    Environment = "Development"
  }

dns_aliases   = ["us-east-1-pg-bank-connector-rw1"]
}

module "pg-bank-connector2" {
  source        = "../../../../modules/aws/lendico-postgresql"
  environment   = "${var.environment}"
  config        = "${merge(local.config-pg-bank-connector,local.config-pg-bank-connector-ro)}"
  subnet        = "lendico-private-us-east-1d"
  instance_name = "pg-bank-connector2"
  user_data_dir = "../../../../templates/user_data"

  region = "us-east-1"

  tags = {
    Service     = "postgres"
    #CostCenter  = "Billing"
    Environment = "Development"
  }

dns_aliases   = ["us-east-1-pg-bank-connector-ro1"]
}

module "pg-bank-connector-dr" {
  source        = "../../../../modules/aws/lendico-postgresql"
  environment   = "${var.environment}"
  config        = "${merge(local.config-pg-bank-connector,local.config-pg-bank-connector-dr)}"
  subnet        = "lendico-private-us-east-1d"
  instance_name = "pg-bank-connector-dr"
  user_data_dir = "../../../../templates/user_data"

  region = "us-east-1"

  tags = {
    Service     = "postgres"
    #CostCenter  = "Billing"
    Environment = "Development"
    pg_type     = "DR"
  }

dns_aliases   = ["us-east-1-pg-bank-connector-cold1"]
}

