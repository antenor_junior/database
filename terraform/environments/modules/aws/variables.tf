#resource "aws_ami" "image" {
#    image_id = "ami-0015b9ef68c77328d"
    #ami = "ami-0015b9ef68c77328d"
#}
#variable "prefix" {}
#variable "key_name" {}
#variable "security_groups" {}
#variable "private_zone_id" {}

variable  prefix {
    type = map
    
    default = {
        lendicodev = ""
    }
}

variable  private_zone_id {
    type = map
    
    default = {
        lendicodev = "Z8OHM4ADWUTLK"
    }
}

variable  security_groups {
    type = map
    
    default = {
        "lendicodev" = {
            "us-east-1" = "sg-060abb64b0f46b429"
            #"us-east-1" = "sg-03ba97db2e0c7332b"
        }
    }
}

variable  key_name {
    type = map
    
    default = {
        "lendicodev" = {
            "us-east-1" = "terraform-dev"
        }
    }
}

variable  image_id {
    type = map
    
    default = {
        "lendicodev" = {
            "us-east-1" = "ami-0015b9ef68c77328d"
        }
    }
}