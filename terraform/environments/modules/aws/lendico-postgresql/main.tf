variable "environment" {}
variable "region" {}
variable "subnet" {
  type    = string
  default = "lendico-private-us-east-1d"
}
variable "instance_name" {}
#variable "image_id" {}
variable "ami" {
  default = ""
}
variable "config" {
  type = map
}

variable "map_additional_volume" {
  default = {
    "0"  = "sdb"
    "1"  = "sdc"
    "2"  = "sdd"
    "3"  = "sde"
    "4"  = "sdf"
    "5"  = "sdg"
    "6"  = "sdh"
    "7"  = "sdi"
    "8"  = "sdj"
    "9"  = "sdk"
    "10" = "sdl"
    "11" = "sdm"
    "12" = "sdn"
    "13" = "sdo"
    "14" = "sdp"
    "15" = "sdq"
    "16" = "sdr"
    "17" = "sds"
    "18" = "sdt"
    "19" = "sdu"
    "20" = "sdv"
    "21" = "sdw"
    "22" = "sdx"
    "23" = "sdy"
    "24" = "sdz"
  }
}

variable "tags" {
  type = map

  default = {
    "CostCenter" = "Engineering"
  }
}

variable "default_tags" {
  type = map

  default = {
    "Created By" = "Terraform"
  }
}


variable "user_data_dir" {
  default = "../../templates/user_data"
}

variable "dns_aliases" {
  type = list
  default = []
}

data "template_file" "user_data" {
  template = "${file("${var.user_data_dir}/${lookup(var.config, "user_data")}")}"

  vars = {
    region            = "${var.region}"
    environment       = "${var.environment}"
    service_name      = "${lookup(var.prefix, var.environment)}${var.instance_name}"
    service_role_chef = "${lookup(var.config, "service_role_chef", "")}"
    hostname          = "${lookup(var.prefix, var.environment)}${var.instance_name}.lendico.com.br"
    # pgdata
    volume_pgdata_count    = "${lookup(var.config, "volume_pgdata_count", "1")}"
    volume_pgdata_size     = "${lookup(var.config, "volume_pgdata_size", "200")}"
    volume_pgdata_type     = "${lookup(var.config, "volume_pgdata_type", "gp2")}"
    # pgwal
    volume_pgwal_count     = "${lookup(var.config, "volume_pgwal_count", "0")}"
    volume_pgwal_size      = "${lookup(var.config, "volume_pgwal_size", "200")}"
    volume_pgwal_type      = "${lookup(var.config, "volume_pgwal_type", "gp2")}"
    # backup
    volume_backup_skip     = "${lookup(var.config, "volume_backup_skip", "0")}"
    volume_backup_count    = "${lookup(var.config, "volume_backup_count", "0")}"
    volume_backup_size     = "${lookup(var.config, "volume_backup_size", "500")}"
    volume_backup_type     = "${lookup(var.config, "volume_backup_type", "st1")}"
  }
}

data "aws_subnet" "search" {
  #provider = "aws.${var.environment}"
  #provider = "aws.environment"

  filter {
    name = "tag:Name"

    values = [
      "${var.subnet}",
    ]
  }
}

variable "sg" {
  type    = list
  default = ["dev-postgresql", "bastion" ]
}


data "aws_security_groups" "search" {
  #provider = "aws.${var.environment}"
  #provider = "aws.environment"

  count = "${length(var.sg)}"
  filter {
    name = "tag:Name"

    values = ["${element(var.sg, count.index)}", ]
  }
}

variable "additional_security_groups" {
  type = list
  default = ["sg-03ba97db2e0c7332b", "sg-060abb64b0f46b429"] #
}

variable "comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "evaluation_periods" {
  default = "2"
}

variable "metric_name" {
  default = "StatusCheckFailed_System"
}

variable "namespace" {
  default = "AWS/EC2"
}

variable "period" {
  default = "60"
}

variable "statistic" {
  default = "Minimum"
}

variable "threshold" {
  default = "1"
}

variable "alarm_description" {
  default = "This metric auto recovers EC2 instances"
}



resource "aws_instance" "postgresql" {
  #provider                = "aws.${var.environment}"
  #provider                = "aws.environment"
  ami                     = "${var.ami == "" ? lookup(var.image_id[var.environment], var.region, "") : var.ami}"
  #ami                     = "ami-0015b9ef68c77328d"
  instance_type           = "${lookup(var.config, "instance_type")}"
  subnet_id               = "${data.aws_subnet.search.id}"
  #iam_instance_profile    = "${lookup(var.config, "service_nameOUT", " ")}"
  #iam_instance_profile    = "${lookup(var.prefix, var.environment,"")}${lookup(var.config, "service_nameOUT", "")}"
  ebs_optimized           = "${lookup(var.config, "ebs_optimized", "true")}"
  key_name                = "${lookup(var.key_name[var.environment], var.region, "aws-lendico")}"
  #vpc_security_group_ids  = "${element(data.aws_security_groups.search.*.ids, "${length(var.sg)}")}"
  vpc_security_group_ids  = "${var.additional_security_groups}"
  disable_api_termination = "false"
  user_data               = "${data.template_file.user_data.rendered}"


  root_block_device {
    volume_type = "${lookup(var.config, "volume_type", "gp2")}"
    volume_size = "${lookup(var.config, "volume_size", "200")}"
  }

  #lifecycle {
  #  ignore_changes = ["user_data", "ami", "monitoring"]
  #}

  credit_specification {
    cpu_credits = "${substr(lookup(var.config, "instance_type"), 0, 3) == "t2." || substr(lookup(var.config, "instance_type"), 0, 3) == "t3." ? "unlimited" : "standard"}"
  }

  tags = "${merge(tomap({"Name" = "${lookup(var.prefix, var.environment)}${var.instance_name}"}),var.default_tags,var.tags,tomap({"hostname" = "${lookup(var.prefix, var.environment)}${var.instance_name}.lendico.com.br"}),tomap({"Cluster" = "${lookup(var.config, "service_name")}"}))}"

#  provisioner "remote-exec" {
#    inline = ["echo 'Wait until SSH is ready'"]
#
#    connection {
#      type                  = "ssh"
#      agent                 = false
#      user                  = "root"
#      private_key           = file("~/.ssh/terraform-dev")
#      #port                  = "9999"
#      host                  = aws_instance.postgresql.private_ip
#      bastion_host          = "bastion.lks.lendico.net.br"
#      bastion_user          = "bastion_generic_user"
#      bastion_port          = "9999"
#      bastion_private_key   = file(lookup(var.config, "private_key_path"))
#    }
#  }
#  provisioner "local-exec" {
#    command = "ansible-playbook  -i '${aws_instance.postgresql.private_ip},' --private-key ${lookup(var.config, "private_key_path")} ~/lendico/git/ansible/database/pg/pg-bank-connector.yml"
#  }  
}

#output "postgresql_ip" {
#  value = aws_instance.postgresql.private_ip
#}

# pgdata volume
resource "aws_ebs_volume" "pgdata" {
  #provider          = "aws.${var.environment}"
  #provider          = "aws.environment"
  count             = "${lookup(var.config, "volume_pgdata_count", "1")}"
  availability_zone = "${aws_instance.postgresql.availability_zone}"
  size              = "${lookup(var.config, "volume_pgdata_size", "200")}"
  type              = "${lookup(var.config, "volume_pgdata_type", "gp2")}"

  tags = "${merge(tomap({"Name" = "${lookup(var.prefix, var.environment)}${var.instance_name}-pgdata-${lookup(var.map_additional_volume, count.index)}"}),var.default_tags,var.tags,tomap({"Pg_storage" = "pgdata"}) )}"
}

resource "aws_volume_attachment" "pgdata_attachment" {
  #provider    = "aws.${var.environment}"
  #provider    = "aws.environment"
  depends_on  = [aws_ebs_volume.pgdata]
  count       = "${lookup(var.config, "volume_pgdata_count", "1")}"
  device_name = "/dev/${lookup(var.map_additional_volume, count.index)}"
  volume_id   = "${element(aws_ebs_volume.pgdata.*.id, count.index)}"
  instance_id = "${aws_instance.postgresql.id}"
  lifecycle {
    ignore_changes = [device_name]
  }
}

# pgwal volume
resource "aws_ebs_volume" "pgwal" {
  #provider          = "aws.${var.environment}"
  #provider          = "aws.environment"
  count             = "${lookup(var.config, "volume_pgwal_count", "0")}"
  availability_zone = "${aws_instance.postgresql.availability_zone}"
  size              = "${lookup(var.config, "volume_pgwal_size", "200")}"
  type              = "${lookup(var.config, "volume_pgwal_type", "gp2")}"

  #tags = "${merge(tomap({"Name" = "${lookup(var.prefix, var.environment)}${var.instance_name}-pgwal-${lookup(var.map_additional_volume, count.index + lookup(var.config, "volume_pgdata_count", "1"))}"}),var.default_tags,var.tags)}"
  tags = "${merge(tomap({"Name" = "${lookup(var.prefix, var.environment)}${var.instance_name}-pgwal-${lookup(var.map_additional_volume, count.index + lookup(var.config, "volume_pgwal_count", "1"))}"}),var.default_tags,var.tags,tomap({"Pg_storage" = "pgwal"}) )}"
}

resource "aws_volume_attachment" "pgwal_attachment" {
  #provider    = "aws.${var.environment}"
  #provider    = "aws.environment"
  depends_on  = [aws_ebs_volume.pgwal]
  count       = "${lookup(var.config, "volume_pgwal_count", "0")}"
  device_name = "/dev/${lookup(var.map_additional_volume, count.index + lookup(var.config, "volume_pgdata_count", "1"))}"
  volume_id   = "${element(aws_ebs_volume.pgwal.*.id, count.index)}"
  instance_id = "${aws_instance.postgresql.id}"
  lifecycle {
    ignore_changes = [device_name]
  }
}

# backup volume
resource "aws_ebs_volume" "backup" {
  #provider          = "aws.${var.environment}"
  #provider          = "aws.environment"
  count             = "${lookup(var.config, "volume_backup_count", "0")}"
  availability_zone = "${aws_instance.postgresql.availability_zone}"
  size              = "${lookup(var.config, "volume_backup_size", "500")}"
  type              = "${lookup(var.config, "volume_backup_type", "st1")}"

  tags = "${merge(tomap({"Name" = "${lookup(var.prefix, var.environment)}${var.instance_name}-backup-${lookup(var.map_additional_volume, count.index + lookup(var.config, "volume_pgdata_count", "1") + lookup(var.config, "volume_pgwal_count", "0") + lookup(var.config, "volume_backup_skip", "0"))}"}),var.default_tags,var.tags)}"
}

resource "aws_volume_attachment" "backup_attachment" {
  #provider    = "aws.${var.environment}"
  #provider    = "aws.environment"
  depends_on  = [aws_ebs_volume.backup]
  count       = "${lookup(var.config, "volume_backup_count", "0")}"
  device_name = "/dev/${lookup(var.map_additional_volume, count.index + lookup(var.config, "volume_pgdata_count", "1") + lookup(var.config, "volume_pgwal_count", "0") + lookup(var.config, "volume_backup_skip", "0"))}"
  volume_id   = "${element(aws_ebs_volume.backup.*.id, count.index)}"
  instance_id = "${aws_instance.postgresql.id}"
  lifecycle {
    ignore_changes = [device_name]
  }
}

#resource "aws_cloudwatch_metric_alarm" "autorecover" {
#  #provider                  = "aws.${var.environment}"
#  #provider                  = "aws.environment"
#  alarm_name                = "DISASTER_EC2_AUTO_RECOVER_${var.instance_name}"
#  comparison_operator       = "${var.comparison_operator}" 
#  evaluation_periods        = "${var.evaluation_periods}"
#  metric_name               = "${var.metric_name}"
#  namespace                 = "${var.namespace}"
#  period                    = "${var.period}"
#  statistic                 = "${var.statistic}"
#  threshold                 = "${var.threshold}"
#  alarm_description         = "${var.alarm_description}"
#  alarm_actions             = ["arn:aws:automate:${var.region}:ec2:recover","arn:aws:sns:${var.region}:${var.account_id[var.environment]}:PROD_DBA-INFRA-V1"]
#  dimensions {
#      InstanceId        = "${aws_instance.postgresql.id}"
#  }
#}

resource "aws_route53_record" "default-record" {
  count    = "${length(var.dns_aliases)}"
  #provider = "aws.environment" #+ "aws.region"
  zone_id  = "${lookup(var.private_zone_id, var.environment)}"
  name     = "${var.dns_aliases[count.index]}"
  #type     = "CNAME"
  type     = "A"
  ttl      = "60"
  #records  = ["${lookup(var.prefix, var.environment)}${var.instance_name}.lendico.com.br"]
  records  = [aws_instance.postgresql.private_ip]
}

resource "aws_route53_record" "alias" {
  count    = "${length(var.dns_aliases)}"
  #provider = "aws.environment" #+ "aws.region"
  zone_id  = "${lookup(var.private_zone_id, var.environment)}"
  name     = "${var.instance_name}"
  #type     = "CNAME"
  type     = "A"
  ttl      = "60"
  #records  = ["${lookup(var.prefix, var.environment)}${var.instance_name}.lendico.com.br"]
  records  = [aws_instance.postgresql.private_ip]
}
