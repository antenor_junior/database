#/bin/bash

#PG MULTI-SCHEMA -> bash $(find ~ -name loop_create_service.sh)  "8880" "PG" "DROP" "MULTI" 2>&1 | tee -a /tmp/loop_create_service.log
#PG MULTI-BASE -> bash $(find ~ -name loop_create_service.sh) "8880" "PG" "DROP" "" 2>&1 | tee -a /tmp/loop_create_service.log
set -e

PG_BIN=$(find /usr -path '*/bin/*' 2>&- | grep psql | sed 's/psql//' | tail -1)
PORT=$1
ENGINE=$2
#DATABASE="jupiter;krypton;mercurio;plutao;titan;qa;hlg"
DATABASE="mercurio"
MYSQL_SCHEMA="api;approval;backoffice;communication;contract-manager;lists;payments;pricing;serasa-services;users;user-communication;user-profile;blacklist;bpc-leads;cbss-services;cities;claris;plus;referrals;users"
#PG_SCHEMA="bank-slip-services;workflow;banking-services;campaign-manager;cashflow;cdc-backoffice;cdc-engine;cdc-manager;cdc-sorocred-services;history-analysis;inbound-api;score;metabase;serasa-services;metrics-publisher;notifier;outbound-api;sorocred-services;recupera-services"
PG_SCHEMA="bank-slip-services"
DB_QTD=1

if [ $ENGINE == "MYSQL" ]; then
  SCHEMA=$MYSQL_SCHEMA
else
  SCHEMA=$PG_SCHEMA
fi

if [ $3 == "DROP" ];
then
  #loop DB
  while [ $DB_QTD -le $( echo $DATABASE | awk -F ";" '{print NF}') ];
  do
    SCHEMA_QTD=1;
    DB_NAME=$( echo $DATABASE | awk -F ";" '{print $'$DB_QTD'}')
    #PG_SCHEMA=$(echo $DB | sed 's/lendico-//g' )
	#DB_PG=$DB
    #if [ "$2" ]; then
    #  DB_PG=$2
    #fi  
	
	if [ "$4" == "" ]; then
	  #loop SCHEMA
      while [ $SCHEMA_QTD -le $( echo $SCHEMA | awk -F ";" '{print NF}') ];
      do
        SCHEMA_NAME=$( echo $SCHEMA | awk -F ";" '{print $'$SCHEMA_QTD'}')
        #DB_NAME=$( echo $DATABASE | awk -F ";" '{print $'$DB_QTD'}')"-"$( echo $SCHEMA | awk -F ";" '{print $'$SCHEMA_QTD'}')
        echo "DROP database "$DB_NAME"-"$SCHEMA_NAME " on " $ENGINE
        echo "===================================="
        echo "DROP service " $DB_NAME
        $PG_BIN/psql  -p $PORT -h 127.0.0.1 -U root -v service_name=$DB_NAME -v db_name=$DB_NAME"-"$SCHEMA_NAME -f $(find ~ -name drop_service_db.sql)  postgres
        echo "===================================="  
        let SCHEMA_QTD=SCHEMA_QTD+1
      done
    else
      echo "DROP database "$DB_NAME " on " $ENGINE
      echo "===================================="
      $PG_BIN/psql  -p $PORT -h 127.0.0.1 -U root -v service_name=$DB_NAME -v db_name=$DB_NAME -f $(find ~ -name drop_service_db.sql) postgres
      echo "===================================="  
    fi
  let DB_QTD=DB_QTD+1
  done

else

  for DB in $(cat /home/centos/mysql_db.csv)
  do
	DB_PG=$( echo $DB | awk -F "-" '{print $NF}')
    if [ "$2" ]; then
      DB_PG=$2
    fi

    for SCHEMA in $(cat /home/centos/mysql_db.csv)	
    do
      #PG_SCHEMA=$(echo $SCHEMA | sed 's/lendico-//g' | sed 's/-'$DB_PG'//g' )
      PG_SCHEMA=$(echo $SCHEMA | sed 's/lendico-//g' | sed 's/-'$( echo $SCHEMA | awk -F "-" '{print $NF}')'//g' )
      echo "===================================="
      echo "Create service " $DB " on schema " $PG_SCHEMA
      sudo -iu postgres $PG_BIN/psql -v service_name=$PG_SCHEMA -v db_name=$DB_PG -f /lendico/postgresql/scripts/create_service_db_multi_schema.sql
      echo "===================================="
    done
  done

fi

