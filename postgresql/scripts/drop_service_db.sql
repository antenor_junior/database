--\set service_name :db_name
\set service_dml_role :service_name'-dml'
\set service_ddl_role :service_name'-ddl'
\set service_adm_role :service_name'-admin'
\set service_ro_role :service_name'-ro'
\set service_app_user :service_name'-app'
\set service_user :service_name


-- The database
\echo 'SET ROLE ':"service_name"
SET ROLE :"service_name";
\echo 'DROPING database ':"db_name"
DROP DATABASE :"db_name";

\echo 'RESET ROLE to drop roles'
RESET ROLE;

\echo 'DROPING role ':"service_adm_role"
DROP ROLE :"service_adm_role" ;
DROP ROLE :"service_ddl_role" ;
DROP ROLE :"service_dml_role" ;
DROP ROLE :"service_ro_role" ;
DROP ROLE :"service_app_user" ;
DROP ROLE :"service_user" ;

