\set service_name :db_name
\set service_dml_role :service_name'-dml'
\set service_ddl_role :service_name'-ddl'
\set service_adm_role :service_name'-admin'
\set service_ro_role :service_name'-ro'
\set service_app_user :service_name'-app'



-- Gera uma senha para o service_app_user
SELECT string_agg(chars, '' ORDER BY random()) AS pwd
FROM (
    SELECT
        unnest(
            array(SELECT * FROM unnest(upper||upper||upper||upper||upper) ORDER BY random() LIMIT 3+floor(random()*3)::int)
            || array(SELECT * FROM unnest(lower||lower||lower||lower||lower) ORDER BY random() LIMIT 3+floor(random()*3)::int)
            || array(SELECT * FROM unnest(special||special) ORDER BY random() LIMIT 2+floor(random()*2)::int)
            || array(SELECT * FROM unnest(digit||digit) ORDER BY random() LIMIT 2+floor(random()*4)::int)
        )
        AS chars
    FROM (
        SELECT
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[A-Z]') AS upper,
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[a-z]') AS lower,
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[0-9]') AS digit,
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[!#$%&*+-/;?]') AS special
        FROM
            generate_series(33, 122) AS t1(i)
    ) t2
) t3 \gset service_app_user_

\echo 'Creating service ':service_name' with ':service_dml_role', ':service_ro_role', ':service_app_user

BEGIN;
-- Role that owns all objects
\echo 'Creating role ':"service_name"
CREATE ROLE :"service_name" NOLOGIN;
-- Admin role
\echo 'Creating role ':"service_adm_role"
CREATE ROLE :"service_adm_role" ROLE :"USER" IN ROLE :"service_name" NOINHERIT NOLOGIN;
-- DML role
\echo 'Creating role ':"service_dml_role"
CREATE ROLE :"service_dml_role" NOLOGIN;
-- DDL role
\echo 'Creating role ':"service_ddl_role"
CREATE ROLE :"service_ddl_role"  IN ROLE :"service_dml_role", :"service_adm_role" NOLOGIN;
-- Read-only role
\echo 'Creating role ':"service_ro_role"
CREATE ROLE :"service_ro_role" NOLOGIN;
-- App user
\echo 'Creating user ':"service_app_user"' with password ':"service_app_user_pwd"
CREATE ROLE :"service_app_user" LOGIN PASSWORD :'service_app_user_pwd' IN ROLE :"service_dml_role";

--DEV environment - pass same user name
ALTER ROLE :"service_app_user" password :'service_app_user';
COMMIT;

-- The database
\echo 'Creating database ':"db_name"
SELECT '' AS create_db_new \gset 
--SELECT DISTINCT 'CREATE DATABASE :"db_name";' AS new  FROM pg_database WHERE 0 = (SELECT count(datname) FROM pg_database WHERE datname = ':"db_name"' ) \gset create_db_
SELECT DISTINCT 'CREATE DATABASE :"db_name" OWNER :"db_name";'  AS new  FROM pg_database WHERE 0 = (SELECT count(datname) FROM pg_database WHERE datname = ':"db_name"' ) \gset create_db_
:create_db_new

\c :"db_name"
SET ROLE :"service_name";

\echo 'Creating schema ':"schema_name"
SELECT replace(:'schema_name', '-', '_') AS schema_name \gset
CREATE SCHEMA IF NOT EXISTS :"schema_name";
--ALTER SCHEMA :schema_name OWNER TO :"service_name";

-- Setup permissions
\echo 'Setting up permissions'

BEGIN;
--REVOKE CREATE ON SCHEMA public FROM PUBLIC;
--REVOKE CREATE ON DATABASE :"db_name" FROM PUBLIC;
--REVOKE USAGE, CREATE ON SCHEMA public FROM :"service_name";
--CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
--SET ROLE :"service_name";

--ALTER DEFAULT PRIVILEGES IN SCHEMA :"schema_name" GRANT SELECT ON TABLES TO :"service_ro_role";
--ALTER DEFAULT PRIVILEGES IN SCHEMA :"schema_name" GRANT SELECT ON SEQUENCES TO :"service_ro_role";
--ALTER DEFAULT PRIVILEGES IN SCHEMA :"schema_name" GRANT SELECT,INSERT,UPDATE,DELETE ON TABLES TO :"service_dml_role";
--ALTER DEFAULT PRIVILEGES IN SCHEMA :"schema_name" GRANT USAGE, SELECT ON SEQUENCES TO :"service_dml_role";
--ALTER DEFAULT PRIVILEGES IN SCHEMA :"schema_name" GRANT EXECUTE ON FUNCTIONS TO :"service_dml_role";

ALTER DEFAULT PRIVILEGES GRANT SELECT ON TABLES TO :"service_ro_role";
ALTER DEFAULT PRIVILEGES GRANT SELECT ON SEQUENCES TO :"service_ro_role";
ALTER DEFAULT PRIVILEGES GRANT SELECT,INSERT,UPDATE,DELETE ON TABLES TO :"service_dml_role";
ALTER DEFAULT PRIVILEGES GRANT USAGE, SELECT ON SEQUENCES TO :"service_dml_role";
ALTER DEFAULT PRIVILEGES GRANT EXECUTE ON FUNCTIONS TO :"service_dml_role";
ALTER DEFAULT PRIVILEGES GRANT USAGE ON SCHEMAS TO :"service_dml_role", :"service_ro_role";

--SET ROLE postgres;
--DROP SCHEMA IF EXISTS public CASCADE;
COMMIT;

-- ALTER DATABASE :"service_name" SET search_path = :schema_name;
