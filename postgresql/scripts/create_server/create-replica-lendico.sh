#!/bin/sh

PRIMARY_HOST="$1"

if [ -z "${PRIMARY_HOST}" -a -r /lendico/postgresql/pg_primary_host ]; then
    PRIMARY_HOST="$( cat /lendico/postgresql/pg_primary_host )"
fi

if [ -z "${PG_ROUTE_DNS}" -a -r /lendico/postgresql/pg_route_dns ]; then
    PG_ROUTE_DNS="$( cat /lendico/postgresql/pg_route_dns )"
fi

echo  "$( hostname -f ) is my HOSTNAME!"

if [ $( hostname -s ) = ${PRIMARY_HOST} ]; then
	echo "PRIMARY_HOST it's that! Aborting..."
	exit 1
fi

if [ -z "${PRIMARY_HOST}" ]; then
    echo "PRIMARY_HOST not defined! Aborting..."
    exit 1
fi

if [ -z "${REPLICATION_SLOT}" ]; then
    REPLICATION_SLOT=true
fi

log() {
    echo "$( date +'%F %T %z' ) - $@"
}

set -ex

#refname="$( hostname -s | sed 's/-/_/g' )"
refname="$( echo ${PRIMARY_HOST} | sed 's/-/_/g' )"

pgversion="`sudo cat /lendico/postgresql/pg_version | sort -rV | head -n1`"
waldirname=pg_wal
waldirparam=--waldir
if sudo test -e /data/${pgversion}/pgdata/pg_xlog -o -h /data/${pgversion}/pgdata/pg_xlog; then
    waldirname=pg_xlog
    waldirparam=--xlogdir
fi

sudo mkdir -p /data/${pgversion}
sudo chown postgres. /data/${pgversion}

pgbasebackuplogdir=""
if ( grep -q 'LABEL=WAL' /etc/fstab ); then
    has_wal_volume=yes
    pgbasebackuplogdir="${waldirparam} /wal/${pgversion}/${waldirname}-restore"
    sudo mkdir -p /wal/${pgversion}
    sudo chown postgres. /wal/${pgversion}
fi

if [ "${REPLICATION_SLOT}" = "true" ]; then
    log "Creating replication slot on primary"
    counter=0

    while ! sudo -iu postgres psql -X -h "${PRIMARY_HOST}.${PG_ROUTE_DNS}" -U replicacao -d postgres -c "SELECT * FROM pg_create_physical_replication_slot('${refname}', true)" ; do
        sleep 30
        counter=$((counter + 1))
         if [ $counter -ge 10 ]; then
             exit
         fi
    done
fi


# Remove Chef scheduler, so it don't start PG while we are syncing (the cron job will be recreated when chef client is run on the last step bellow)
if [ -e /etc/cron.d/chef-client ]; then
    sudo rm /etc/cron.d/chef-client
fi

log "Starting pg_basebackup"
if [ "${REPLICATION_SLOT}" = "true" ]; then
    sudo -iu postgres pg_basebackup -P -R -c fast -X stream -h "${PRIMARY_HOST}.${PG_ROUTE_DNS}" -U replicacao -S "${refname}" -l "${refname}" -D /data/${pgversion}/pgdata-restore ${pgbasebackuplogdir}
else
    sudo -iu postgres pg_basebackup -P -R -c fast -h "${PRIMARY_HOST}.${PG_ROUTE_DNS}" -U replicacao -D /data/${pgversion}/pgdata-restore ${pgbasebackuplogdir}
fi

# Configure recovery targets, so it stops as soon as consistent state has reached
if [ ${pgversion} -ge "12" ]; then #PG 12 and above configuration
    sudo -iu postgres mkdir -p /data/${pgversion}/pgdata-restore/lendico_conf.d
    sudo -iu postgres cat /data/${pgversion}/pgdata-restore/postgresql.auto.conf | grep '^\(primary\)' > /data/${pgversion}/pgdata-restore/lendico_conf.d/recovery_lendico.conf
    sudo chown postgres: /data/${pgversion}/pgdata-restore/lendico_conf.d/recovery_lendico.conf
    sudo -iu postgres sed -i '/^\(primary_conninfo\|primary_slot_name\)/d' /data/${pgversion}/pgdata-restore/postgresql.auto.conf
    echo -e "\nrecovery_target_action = 'shutdown'\n" | sudo -iu postgres tee -a /data/${pgversion}/pgdata-restore/lendico_conf.d/recovery_lendico.conf
    echo -e "\ninclude_dir 'lendico_conf.d'\n" | sudo -iu postgres tee -a /data/${pgversion}/pgdata-restore/postgresql.conf
else	
    echo -e "recovery_target = 'immediate'\nrecovery_target_action = 'shutdown'\n" | sudo -iu postgres tee -a /data/${pgversion}/pgdata-restore/recovery.conf
fi

log "pg_basebackup finishing, swapping the instances"
sudo systemctl stop postgresql-${pgversion} || echo "stop failed with code $?"

if ( sudo systemctl status postgresql-${pgversion} ); then
    log "PostgreSQL still running... Aborting!!!"
    exit 1
fi

if ( sudo test -d /data/${pgversion}/pgdata ); then
    sudo -iu postgres mv /data/${pgversion}/pgdata /data/${pgversion}/pgdata-old
fi
sudo -iu postgres mv /data/${pgversion}/pgdata-restore /data/${pgversion}/pgdata
sudo chmod -R 700 /data/${pgversion}

# Rename WAL directory, if needed
if [ x"${has_wal_volume}" == x"yes" ]; then
    if ( sudo test -d /wal/${pgversion}/${waldirname} ); then
        sudo -iu postgres mv /wal/${pgversion}/${waldirname} /wal/${pgversion}/${waldirname}-old
    fi
    
    sudo -iu postgres mv /wal/${pgversion}/${waldirname}-restore /wal/${pgversion}/${waldirname}
    sudo -iu postgres rm /data/${pgversion}/pgdata/${waldirname}
    sudo -iu postgres ln -s /wal/${pgversion}/${waldirname} /data/${pgversion}/pgdata/${waldirname}
fi

# Tuning initial
#sudo -iu postgres sed -i '/^\(shared_buffers\)/d' /data/${pgversion}/pgdata/postgresql.conf
#ruby /lendico/git/lendico-database-scripts/postgres/utils/calc_shared_buffers.rb "$( awk '/MemTotal/ {print $2}' /proc/meminfo )" ${pgversion}

if [ ${pgversion} -ge "12" ]; then 
    sudo systemctl start postgresql-${pgversion}
else
    # Start it and wait the recovery to reach a consistent point (this will block until finished)
    sudo -iu postgres /usr/pgsql-${pgversion}/bin/postmaster --logging-collector=off -D /data/${pgversion}/pgdata/
fi

# Remove recovery target (so it will keep replicating)
if [ ${pgversion} -ge "12" ]; then #PG 12 and above configuration
    sudo -iu postgres sed -i '/^\(recovery_target_action\)/d' /data/${pgversion}/pgdata/lendico_conf.d/recovery_lendico.conf
else
    sudo -iu postgres sed -i '/^\(recovery_target\|recovery_target_action\)/d' /data/${pgversion}/pgdata/recovery.conf

    echo -e "recovery_target_timeline = 'latest'" | sudo -iu postgres tee -a /data/${pgversion}/pgdata/recovery.conf
fi

# Run Chef, this will configure the .conf accordingly and start PostgreSQL
#sudo chef
#sudo systemctl restart postgresql-${pgversion}
