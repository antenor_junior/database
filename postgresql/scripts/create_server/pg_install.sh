#vim /lendico/postgresql/scripts/pg_install.sh

#/bin/bash
set -xe

#bash pg_install.sh "pg-poc1" "MASTER" "13"
sudo mkdir -p /lendico /lendico/postgresql /lendico/postgresql/scripts/
if [ $(cat /lendico/postgresql/pg_version | wc -c) <= "1" ]; then
  sudo echo $3 > /lendico/postgresql/pg_version
fi

if [ $(cat /lendico/postgresql/pg_primary_host | wc -c) <= "1" ]; then
  sudo echo $1 > /lendico/postgresql/pg_primary_host
fi

PG_VERSION=$(sudo cat /lendico/postgresql/pg_version)
sudo yum install epel-release -y
sudo yum install -y postgresql$PG_VERSION-server postgresql$PG_VERSION-contrib postgresql$PG_VERSION python3 python3-pip fabric iotop screen

#llvm-toolset-7-clang llvm5.0-devel
#sudo chown -R postgres:  /lendico

sudo systemctl stop postgresql-$PG_VERSION
sudo rm -rf /data/$PG_VERSION/
sudo rm -rf /wal/$PG_VERSION/

if [ -d "/wal" ]; then
  sudo -iu postgres /usr/pgsql-$PG_VERSION/bin/initdb -D /data/$PG_VERSION/pgdata/ -X /wal/$PG_VERSION/pg_wal/
else
  sudo -iu postgres /usr/pgsql-$PG_VERSION/bin/initdb -D /data/$PG_VERSION/pgdata/
fi
sudo -iu postgres  echo "*:*:*:replicacao:123456" | sudo -iu postgres tee .pgpass
sudo -iu postgres chmod 600 .pgpass
sudo sed -i 's/PGDATA=\/var\/lib\/pgsql\/'$PG_VERSION'\/data/PGDATA=\/data\/'$PG_VERSION'\/pgdata/' /usr/lib/systemd/system/postgresql-$PG_VERSION.service
sudo systemctl daemon-reload
sudo systemctl enable postgresql-$PG_VERSION
 

#MEM=$(free -b | grep Mem | awk {'print $2'})
#PG_SB=$(echo $MEM "* 0.9" | bc | awk -F . {'print$1'} | numfmt --to=iec --suffix=B)
#echo "shared_buffers = " $PG_SB > /data/$PG_VERSION/pgdata/postgresql.conf
 
sudo rm -rf /data/$PG_VERSION/pgdata/postgresql.conf
sudo cp /lendico/postgresql/postgresql.conf /data/$PG_VERSION/pgdata/

sudo echo "data_directory = '/data/$PG_VERSION/pgdata/'" | sudo tee -a /data/$PG_VERSION/pgdata/postgresql.conf
sudo echo "config_file = '/data/$PG_VERSION/pgdata/postgresql.conf'" | sudo tee -a /data/$PG_VERSION/pgdata/postgresql.conf
sudo yum install pgtune -y
sudo pgtune -i /data/$PG_VERSION/pgdata/postgresql.conf -o /data/$PG_VERSION/pgdata/postgresql.conf -T OLTP -c 800
sudo sed -i '/checkpoint_segments/d' /data/$PG_VERSION/pgdata/postgresql.conf

sudo chown -R postgres:  /data
sudo chown -R postgres:  /wal

sudo systemctl start postgresql-$PG_VERSION

sudo -iu postgres psql -c "CREATE ROLE replicacao LOGIN REPLICATION"
