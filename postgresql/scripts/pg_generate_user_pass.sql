--psql -h localhost -d postgres -U root -v service_app_user=u -v service_dml_role=u_dml -f ~/lendico/git/database/postgresql/scripts/pg_user_pass.sql

\set service_dml_role :service_name'-dml'
\set service_ddl_role :service_name'-ddl'
\set service_adm_role :service_name'-admin'
\set service_ro_role :service_name'-ro'
\set service_app_user :service_name'-app'

-- Gera uma senha para o service_app_user
SELECT string_agg(chars, '' ORDER BY random()) AS pwd
FROM (
    SELECT
        unnest(
            array(SELECT * FROM unnest(upper||upper||upper||upper||upper) ORDER BY random() LIMIT 3+floor(random()*3)::int)
            || array(SELECT * FROM unnest(lower||lower||lower||lower||lower) ORDER BY random() LIMIT 3+floor(random()*3)::int)
            || array(SELECT * FROM unnest(special||special) ORDER BY random() LIMIT 2+floor(random()*2)::int)
            || array(SELECT * FROM unnest(digit||digit) ORDER BY random() LIMIT 2+floor(random()*4)::int)
        )
        AS chars
    FROM (
        SELECT
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[A-Z]') AS upper,
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[a-z]') AS lower,
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[0-9]') AS digit,
            array_agg(chr(i)) FILTER(WHERE chr(i) ~ '[!#$%&*+-/;?]') AS special
        FROM
            generate_series(33, 122) AS t1(i)
    ) t2
) t3 \gset service_app_user_

--\echo 'Creating service ':service_name' with ':service_dml_role', ':service_ro_role', ':service_app_user


--BEGIN;
-- App user
--\echo 'Creating user ':"service_app_user"' with password ':"service_app_user_pwd"
\echo :service_app_user_pwd
--CREATE ROLE :"service_app_user" LOGIN PASSWORD :'service_app_user_pwd' IN ROLE :"service_dml_role";
--COMMIT;