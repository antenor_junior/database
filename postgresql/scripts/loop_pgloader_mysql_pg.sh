#/bin/bash
#bash /lendico/postgresql/scripts/loop_pgloader_mysql_pg.sh  2>&1 | tee -a /tmp/pgloader.run
set -ex

PG_BIN=$(sudo find /usr -path '*/bin' 2>&- | grep pgsql | tail -1)

#DATABASE="jupiter;krypton;mercurio;plutao;titan;qa;hlg"
#MYSQL_SCHEMA="api;approval;backoffice;communication;contract-manager;lists;payments;pricing;serasa-services;users;user-communication;user-profile;blacklist;bpc-leads;cbss-services;cities;claris;plus;referrals;users"
DATABASE="qa"
MYSQL_SCHEMA="api"
DB_QTD=1;
#loop create DB
while [ $DB_QTD -le $( echo $DATABASE | awk -F ";" '{print NF}') ];
do

  SCHEMA_QTD=1;
  while [ $SCHEMA_QTD -le $( echo $MYSQL_SCHEMA | awk -F ";" '{print NF}') ];
  do
    DB_MYSQL=$( echo "lendico-"$MYSQL_SCHEMA | awk -F ";" '{print $'$SCHEMA_QTD'}')"-"$( echo $DATABASE | awk -F ";" '{print $'$DB_QTD'}')
    PG_SCHEMA=$( echo $MYSQL_SCHEMA | awk -F ";" '{print $'$SCHEMA_QTD'}' | sed 's/-/_/g' )"_my"
    DB_PG=$( echo $DATABASE | awk -F ";" '{print $'$DB_QTD'}')
	DB_LOAD=$(echo $DB_MYSQL | sed 's/-/_/g' )
#for DB in $(cat /home/centos/mysql_db.csv)
#do
#  PG_SCHEMA=$(echo $DB | sed 's/lendico-//g' | sed 's/-/_/g' )"_my"
#  DB_LOAD=$(echo $DB | sed 's/-/_/g' )
#  DB_PG=$DB
#  if [ $1 ]; then
#    DB_PG=$1
#  fi
    echo "===================================="
    echo "create LOAD file for DB " $DB_LOAD
    echo "LOAD DATABASE" > $DB_LOAD.load
    echo "  FROM mysql://pgloader:pgloader@lendico-mysql-db.clecbdjbx49l.us-east-1.rds.amazonaws.com:3306/"$DB_MYSQL >> $DB_LOAD.load
    echo "  INTO postgresql://antenor.junior:123456@localhost:5432/"$DB_PG >> $DB_LOAD.load
    echo "CAST type text to jsonb," >> $DB_LOAD.load
    echo "          type mediumtext to jsonb," >> $DB_LOAD.load
    echo "          type longtext to jsonb," >> $DB_LOAD.load
    echo "          type datetime to \"timestamp without time zone\"" >> $DB_LOAD.load
    echo "ALTER TABLE NAMES MATCHING ~/./  SET SCHEMA '"$PG_SCHEMA"';" >> $DB_LOAD.load
    #echo $DB_LOAD
    #echo $DB
    #exit 0
    pgloader --logfile "/tmp/mysql_pg_$DB_LOAD.log" $DB_LOAD.load
    
    echo "===================================="  
    echo "mysql schema"  
    echo "===================================="  
    mysqldump -dfP 3306 -h lendico-mysql-db.clecbdjbx49l.us-east-1.rds.amazonaws.com -u pgloader --single-transaction --ssl-mode=DISABLED --column-statistics=0 --databases $DB_MYSQL > /tmp/pgloader_$DB_MYSQL\_mysql_schema.sql
    echo "===================================="  
    echo "pg schema"  
    echo "===================================="  
    $PG_BIN/pg_dump -sh localhost -p 5432 -n $PG_SCHEMA -U antenor.junior -f /tmp/pgloader_$DB_PG\_$PG_SCHEMA\_pg_schema.sql $DB_PG
    echo "===================================="  
    let SCHEMA_QTD=SCHEMA_QTD+1
  done
 
 let DB_QTD=DB_QTD+1
done